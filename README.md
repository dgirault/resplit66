ReloadedSplit66
===============

This is a split ergonomic keyboard, initially based on [Splitty](https://github.com/schodet/splitty),
but with so much changes that it was redesign from scratch.

Key features of ReloadedSplit66:

- 33 Cherry MX compatible keys per side, with 4 1.5u caps, two vertical and two
  horizontal.
- 3 onboard 3mm standard leds for Caps/Num/Scroll lock indication.
- The three index columns are aligned, auricular columns also.
- The column offsets are -16mm for the little finger, -6mm for the ring finger 
  and -8mm for the index finger.
- Integrated controller using a STM32L443CCTx MCU (or STM32F303CCTx if you want).
- USB Type-C connector + 6 pins 1,27mm header for USB HUB option.
- USB Micro-B connector for master-slave connection (I2C or UART if cross-cable).
- Standard JTAG connector (J3) for debugging.
- Uart (J4), SPI/I2C/SAI/GPIO (J5), CAN/GPIO (J7) connector for plenty extensions 
  possibilities.
- The same PCB is used for both hands.


![ReSplit66 with keys and caps](img/freecad.png)

![ReSplit66 PCB top](img/resplit66_pcb_top.png)

![ReSplit66 PCB bottom](img/resplit66_pcb_bottom.png)

![ReSplit66 Layout](img/kle_bepo.png)


More pictures in the [gallery](gallery.md).

Connectors pinout are in [pinout](pinout.md).

License
-------

Copyright (C) 2020 David Girault

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
