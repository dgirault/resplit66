EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Mechanical:MountingHole MK6
U 1 1 5AFD4177
P 10400 5500
F 0 "MK6" H 10400 5700 50  0000 C BNN
F 1 "Mounting_Hole" H 10400 5625 50  0000 C CNN
F 2 "MountingHole:MountingHole_2.2mm_M2" H 10400 5500 50  0001 C CNN
F 3 "" H 10400 5500 50  0001 C CNN
	1    10400 5500
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole MK3
U 1 1 5AFD44A2
P 10400 5150
F 0 "MK3" H 10400 5350 50  0000 C BNN
F 1 "Mounting_Hole" H 10400 5275 50  0000 C CNN
F 2 "MountingHole:MountingHole_2.2mm_M2" H 10400 5150 50  0001 C CNN
F 3 "" H 10400 5150 50  0001 C CNN
	1    10400 5150
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole MK8
U 1 1 5AFD47BD
P 9850 5850
F 0 "MK8" H 9850 6050 50  0000 C BNN
F 1 "Mounting_Hole" H 9850 5975 50  0000 C CNN
F 2 "MountingHole:MountingHole_2.2mm_M2" H 9850 5850 50  0001 C CNN
F 3 "" H 9850 5850 50  0001 C CNN
	1    9850 5850
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole MK5
U 1 1 5AFD4B28
P 9850 5500
F 0 "MK5" H 9850 5700 50  0000 C BNN
F 1 "Mounting_Hole" H 9850 5625 50  0000 C CNN
F 2 "MountingHole:MountingHole_2.2mm_M2" H 9850 5500 50  0001 C CNN
F 3 "" H 9850 5500 50  0001 C CNN
	1    9850 5500
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole MK9
U 1 1 5E1E462B
P 10400 5850
F 0 "MK9" H 10400 6050 50  0000 C BNN
F 1 "Mounting_Hole" H 10400 5975 50  0000 C CNN
F 2 "MountingHole:MountingHole_2.2mm_M2" H 10400 5850 50  0001 C CNN
F 3 "" H 10400 5850 50  0001 C CNN
	1    10400 5850
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole MK7
U 1 1 5AFD4314
P 9300 5850
F 0 "MK7" H 9300 6050 50  0000 C BNN
F 1 "Mounting_Hole" H 9300 5975 50  0000 C CNN
F 2 "MountingHole:MountingHole_2.2mm_M2" H 9300 5850 50  0001 C CNN
F 3 "" H 9300 5850 50  0001 C CNN
	1    9300 5850
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole MK4
U 1 1 5AFD462B
P 9300 5500
F 0 "MK4" H 9300 5700 50  0000 C BNN
F 1 "Mounting_Hole" H 9300 5625 50  0000 C CNN
F 2 "MountingHole:MountingHole_2.2mm_M2" H 9300 5500 50  0001 C CNN
F 3 "" H 9300 5500 50  0001 C CNN
	1    9300 5500
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole MK2
U 1 1 5AFD3B40
P 9850 5150
F 0 "MK2" H 9850 5350 50  0000 C BNN
F 1 "Mounting_Hole" H 9850 5275 50  0000 C CNN
F 2 "MountingHole:MountingHole_2.2mm_M2" H 9850 5150 50  0001 C CNN
F 3 "" H 9850 5150 50  0001 C CNN
	1    9850 5150
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole MK1
U 1 1 5AFD4950
P 9300 5150
F 0 "MK1" H 9300 5350 50  0000 C BNN
F 1 "Mounting_Hole" H 9300 5275 50  0000 C CNN
F 2 "MountingHole:MountingHole_2.2mm_M2" H 9300 5150 50  0001 C CNN
F 3 "" H 9300 5150 50  0001 C CNN
	1    9300 5150
	1    0    0    -1  
$EndComp
Wire Wire Line
	2400 2450 2700 2450
Wire Wire Line
	2400 2850 2700 2850
Wire Wire Line
	2400 2450 2400 2200
$Comp
L Connector_Generic:Conn_02x05_Odd_Even J?
U 1 1 5EB63367
P 2900 2650
AR Path="/5E103213/5EB63367" Ref="J?"  Part="1" 
AR Path="/5EB63367" Ref="J1"  Part="1" 
F 0 "J1" H 2950 3067 50  0000 C CNN
F 1 "RGB_Oled" H 2950 2976 50  0000 C CNN
F 2 "Connector_PinHeader_1.27mm:PinHeader_2x05_P1.27mm_Vertical" H 2900 2650 50  0001 C CNN
F 3 "~" H 2900 2650 50  0001 C CNN
	1    2900 2650
	1    0    0    -1  
$EndComp
Wire Wire Line
	3200 2750 3450 2750
Text Label 2400 2850 0    50   ~ 0
sda
Text Label 3450 2750 2    50   ~ 0
scl
NoConn ~ 2700 2650
Text Notes 1600 1700 0    50   ~ 0
Use the secondary I2C bus exposed by main board on pin 8 & 9.
Wire Wire Line
	8500 2550 8150 2550
Wire Wire Line
	8500 2650 8150 2650
Wire Wire Line
	8150 2750 8500 2750
Wire Wire Line
	8150 2850 8500 2850
Text Label 8150 2550 0    50   ~ 0
j4_1
Text Label 8150 2650 0    50   ~ 0
j4_2
Text Label 8150 2750 0    50   ~ 0
j4_3
Text Label 8150 2850 0    50   ~ 0
j4_4
$Comp
L Mechanical:MountingHole MK10
U 1 1 5FB1798F
P 9600 6250
F 0 "MK10" H 9600 6450 50  0000 C BNN
F 1 "Mounting_Hole" H 9600 6375 50  0000 C CNN
F 2 "MountingHole:MountingHole_2.2mm_M2" H 9600 6250 50  0001 C CNN
F 3 "" H 9600 6250 50  0001 C CNN
	1    9600 6250
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole MK11
U 1 1 5FB17D7A
P 10200 6250
F 0 "MK11" H 10200 6450 50  0000 C BNN
F 1 "Mounting_Hole" H 10200 6375 50  0000 C CNN
F 2 "MountingHole:MountingHole_2.2mm_M2" H 10200 6250 50  0001 C CNN
F 3 "" H 10200 6250 50  0001 C CNN
	1    10200 6250
	1    0    0    -1  
$EndComp
NoConn ~ 2700 2550
NoConn ~ 3200 2550
NoConn ~ 3200 2650
NoConn ~ 2700 2750
Wire Wire Line
	3200 2450 3450 2450
Wire Wire Line
	3450 2450 3450 2200
$Comp
L power:+3V3 #PWR0101
U 1 1 5FCD80EE
P 2400 2200
F 0 "#PWR0101" H 2400 2050 50  0001 C CNN
F 1 "+3V3" H 2415 2373 50  0000 C CNN
F 2 "" H 2400 2200 50  0001 C CNN
F 3 "" H 2400 2200 50  0001 C CNN
	1    2400 2200
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0103
U 1 1 5FCDA1E5
P 3450 2200
F 0 "#PWR0103" H 3450 2050 50  0001 C CNN
F 1 "+5V" H 3465 2373 50  0000 C CNN
F 2 "" H 3450 2200 50  0001 C CNN
F 3 "" H 3450 2200 50  0001 C CNN
	1    3450 2200
	1    0    0    -1  
$EndComp
Wire Wire Line
	3200 2850 3450 2850
Wire Wire Line
	3450 2850 3450 2950
$Comp
L power:GND #PWR0105
U 1 1 5FCDC352
P 3450 2950
F 0 "#PWR0105" H 3450 2700 50  0001 C CNN
F 1 "GND" H 3455 2777 50  0000 C CNN
F 2 "" H 3450 2950 50  0001 C CNN
F 3 "" H 3450 2950 50  0001 C CNN
	1    3450 2950
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x04 J2
U 1 1 5FCE2D51
P 8700 2650
F 0 "J2" H 8780 2642 50  0000 L CNN
F 1 "Oled_128x32" H 8780 2551 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 8700 2650 50  0001 C CNN
F 3 "~" H 8700 2650 50  0001 C CNN
	1    8700 2650
	1    0    0    -1  
$EndComp
Text Notes 5200 1400 0    50   ~ 0
Left/Right and power selectors.
Wire Wire Line
	5350 2850 6000 2850
Wire Wire Line
	5350 2950 5350 2850
$Comp
L power:GND #PWR0106
U 1 1 5FCDCB1A
P 5350 2950
F 0 "#PWR0106" H 5350 2700 50  0001 C CNN
F 1 "GND" H 5355 2777 50  0000 C CNN
F 2 "" H 5350 2950 50  0001 C CNN
F 3 "" H 5350 2950 50  0001 C CNN
	1    5350 2950
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0104
U 1 1 5FCDAD21
P 5650 2200
F 0 "#PWR0104" H 5650 2050 50  0001 C CNN
F 1 "+5V" H 5665 2373 50  0000 C CNN
F 2 "" H 5650 2200 50  0001 C CNN
F 3 "" H 5650 2200 50  0001 C CNN
	1    5650 2200
	1    0    0    -1  
$EndComp
Wire Wire Line
	5550 2200 5650 2200
$Comp
L power:+3V3 #PWR0102
U 1 1 5FCD8D08
P 5050 2200
F 0 "#PWR0102" H 5050 2050 50  0001 C CNN
F 1 "+3V3" H 5065 2373 50  0000 C CNN
F 2 "" H 5050 2200 50  0001 C CNN
F 3 "" H 5050 2200 50  0001 C CNN
	1    5050 2200
	1    0    0    -1  
$EndComp
$Comp
L Jumper:SolderJumper_3_Open JP5
U 1 1 5EE5C63D
P 6150 3850
F 0 "JP5" V 6104 3918 50  0000 L CNN
F 1 "SCL" V 6195 3918 50  0000 L CNN
F 2 "Jumper:SolderJumper-3_P1.3mm_Open_RoundedPad1.0x1.5mm" H 6150 3850 50  0001 C CNN
F 3 "~" H 6150 3850 50  0001 C CNN
	1    6150 3850
	0    1    1    0   
$EndComp
$Comp
L Jumper:SolderJumper_3_Open JP4
U 1 1 5EE4FA4B
P 6150 3350
F 0 "JP4" V 6104 3418 50  0000 L CNN
F 1 "SDA" V 6195 3418 50  0000 L CNN
F 2 "Jumper:SolderJumper-3_P1.3mm_Open_RoundedPad1.0x1.5mm" H 6150 3350 50  0001 C CNN
F 3 "~" H 6150 3350 50  0001 C CNN
	1    6150 3350
	0    1    1    0   
$EndComp
$Comp
L Jumper:SolderJumper_3_Open JP3
U 1 1 5EE34CBA
P 6150 2850
F 0 "JP3" V 6104 2918 50  0000 L CNN
F 1 "GND" V 6195 2918 50  0000 L CNN
F 2 "Jumper:SolderJumper-3_P1.3mm_Open_RoundedPad1.0x1.5mm" H 6150 2850 50  0001 C CNN
F 3 "~" H 6150 2850 50  0001 C CNN
	1    6150 2850
	0    1    1    0   
$EndComp
Wire Wire Line
	5350 2350 6000 2350
$Comp
L Jumper:SolderJumper_3_Open JP2
U 1 1 5EE209E8
P 6150 2350
F 0 "JP2" V 6104 2418 50  0000 L CNN
F 1 "VCC" V 6195 2418 50  0000 L CNN
F 2 "Jumper:SolderJumper-3_P1.3mm_Open_RoundedPad1.0x1.5mm" H 6150 2350 50  0001 C CNN
F 3 "~" H 6150 2350 50  0001 C CNN
	1    6150 2350
	0    1    1    0   
$EndComp
Wire Wire Line
	5050 2200 5150 2200
$Comp
L Jumper:SolderJumper_3_Open JP1
U 1 1 5EE209DE
P 5350 2200
F 0 "JP1" H 5350 2405 50  0000 C CNN
F 1 "VCC" H 5350 2314 50  0000 C CNN
F 2 "Jumper:SolderJumper-3_P1.3mm_Open_RoundedPad1.0x1.5mm" H 5350 2200 50  0001 C CNN
F 3 "~" H 5350 2200 50  0001 C CNN
	1    5350 2200
	1    0    0    -1  
$EndComp
Text Label 5500 3850 0    50   ~ 0
sda
Text Label 5500 3350 0    50   ~ 0
scl
Text Label 6500 4050 2    50   ~ 0
j4_1
Text Label 6500 3650 2    50   ~ 0
j4_4
Text Label 6500 3550 2    50   ~ 0
j4_2
Text Label 6500 2550 2    50   ~ 0
j4_3
Text Label 6500 3150 2    50   ~ 0
j4_3
Text Label 6500 3050 2    50   ~ 0
j4_4
Text Label 6500 2150 2    50   ~ 0
j4_2
Text Label 6500 2650 2    50   ~ 0
j4_1
Wire Wire Line
	5500 3850 6000 3850
Wire Wire Line
	5500 3350 6000 3350
Wire Wire Line
	6150 4050 6500 4050
Wire Wire Line
	6150 3650 6500 3650
Wire Wire Line
	6150 3550 6500 3550
Wire Wire Line
	6150 3150 6500 3150
Wire Wire Line
	6150 2550 6500 2550
Wire Wire Line
	6150 2150 6500 2150
Wire Wire Line
	6150 3050 6500 3050
Wire Wire Line
	6150 2650 6500 2650
$EndSCHEMATC
