ReloadedSplit66 gallery
=======================


## Main board

Some KiCad view first, with keys.

![Top](img/resplit66_top.png)

![Bottom](img/resplit66_bottom.png)

![Details](img/resplit66_top_details.png)

Using KiCad raytracing view.

![Top](img/resplit66_top_ray.png)

![3D](img/resplit66_3d_ray.png)


## Back plate

![Top](back_plate/img/top.png)

![Bottom](back_plate/img/bottom.png)

## Front plate

![Top](front_plate/img/top.png)

![Bottom](front_plate/img/bottom.png)

## OLED Front plate

![Top](oled_plate/img/oled_plate_top.png)


![Bottom](oled_plate/img/oled_plate_bottom.png)

## RGB leds back board

![Top](leds_plate/img/leds_plate.png)

![Bottom](leds_plate/img/leds_plate_bottom.png)

[![Testing with Arduino](leds_plate/img/smalltesting1.jpg)](leds_plate/img/testing1.jpg)
[![Testing power consuption](leds_plate/img/smalltesting2.jpg)](leds_plate/img/testing2.jpg)
[![Testing with switch & caps](leds_plate/img/smalltesting3.jpg)](leds_plate/img/testing3.jpg)

More to come...
