EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 2
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L LED:SK6805 D?
U 1 1 5E6CAAEF
P 7400 1850
AR Path="/5E103213/5E6CAAEF" Ref="D?"  Part="1" 
AR Path="/5E6CAAEF" Ref="D4"  Part="1" 
F 0 "D4" H 7450 2100 50  0000 L CNN
F 1 "SK6805" H 7400 1600 50  0000 L CNN
F 2 "resplit66:LED_SK6805_PLCC4_2.4x2.7mm_P1.3mm_BothSide" H 7450 1550 50  0001 L TNN
F 3 "https://cdn-shop.adafruit.com/product-files/3484/3484_Datasheet.pdf" H 7500 1475 50  0001 L TNN
	1    7400 1850
	1    0    0    -1  
$EndComp
Wire Wire Line
	8250 2500 8600 2500
Wire Wire Line
	9100 2500 9100 2550
$Comp
L LED:SK6805 D?
U 1 1 5E6CAAF8
P 7400 2850
AR Path="/5E103213/5E6CAAF8" Ref="D?"  Part="1" 
AR Path="/5E6CAAF8" Ref="D12"  Part="1" 
F 0 "D12" H 7200 3100 50  0000 L CNN
F 1 "SK6805" H 7100 2600 50  0000 L CNN
F 2 "resplit66:LED_SK6805_PLCC4_2.4x2.7mm_P1.3mm_BothSide" H 7450 2550 50  0001 L TNN
F 3 "https://cdn-shop.adafruit.com/product-files/3484/3484_Datasheet.pdf" H 7500 2475 50  0001 L TNN
	1    7400 2850
	-1   0    0    -1  
$EndComp
$Comp
L LED:SK6805 D?
U 1 1 5E6CAAFE
P 7400 3850
AR Path="/5E103213/5E6CAAFE" Ref="D?"  Part="1" 
AR Path="/5E6CAAFE" Ref="D20"  Part="1" 
F 0 "D20" H 7450 4100 50  0000 L CNN
F 1 "SK6805" H 7400 3600 50  0000 L CNN
F 2 "resplit66:LED_SK6805_PLCC4_2.4x2.7mm_P1.3mm_BothSide" H 7450 3550 50  0001 L TNN
F 3 "https://cdn-shop.adafruit.com/product-files/3484/3484_Datasheet.pdf" H 7500 3475 50  0001 L TNN
	1    7400 3850
	1    0    0    -1  
$EndComp
Wire Wire Line
	8250 4200 8600 4200
Wire Wire Line
	9100 3150 9100 3200
Connection ~ 9100 3200
Wire Wire Line
	8800 2850 8550 2850
Connection ~ 7400 4200
Connection ~ 8250 4200
Wire Wire Line
	8250 4150 8250 4200
Wire Wire Line
	8250 3200 8600 3200
Connection ~ 8250 3200
Connection ~ 9100 2200
$Comp
L LED:SK6805 D?
U 1 1 5E6CAB0E
P 6550 1850
AR Path="/5E103213/5E6CAB0E" Ref="D?"  Part="1" 
AR Path="/5E6CAB0E" Ref="D3"  Part="1" 
F 0 "D3" H 6600 2100 50  0000 L CNN
F 1 "SK6805" H 6550 1600 50  0000 L CNN
F 2 "resplit66:LED_SK6805_PLCC4_2.4x2.7mm_P1.3mm_BothSide" H 6600 1550 50  0001 L TNN
F 3 "https://cdn-shop.adafruit.com/product-files/3484/3484_Datasheet.pdf" H 6650 1475 50  0001 L TNN
	1    6550 1850
	1    0    0    -1  
$EndComp
$Comp
L LED:SK6805 D?
U 1 1 5E6CAB14
P 6550 2850
AR Path="/5E103213/5E6CAB14" Ref="D?"  Part="1" 
AR Path="/5E6CAB14" Ref="D11"  Part="1" 
F 0 "D11" H 6350 3100 50  0000 L CNN
F 1 "SK6805" H 6250 2600 50  0000 L CNN
F 2 "resplit66:LED_SK6805_PLCC4_2.4x2.7mm_P1.3mm_BothSide" H 6600 2550 50  0001 L TNN
F 3 "https://cdn-shop.adafruit.com/product-files/3484/3484_Datasheet.pdf" H 6650 2475 50  0001 L TNN
	1    6550 2850
	-1   0    0    -1  
$EndComp
$Comp
L LED:SK6805 D?
U 1 1 5E6CAB1A
P 6550 3850
AR Path="/5E103213/5E6CAB1A" Ref="D?"  Part="1" 
AR Path="/5E6CAB1A" Ref="D19"  Part="1" 
F 0 "D19" H 6600 4100 50  0000 L CNN
F 1 "SK6805" H 6550 3600 50  0000 L CNN
F 2 "resplit66:LED_SK6805_PLCC4_2.4x2.7mm_P1.3mm_BothSide" H 6600 3550 50  0001 L TNN
F 3 "https://cdn-shop.adafruit.com/product-files/3484/3484_Datasheet.pdf" H 6650 3475 50  0001 L TNN
	1    6550 3850
	1    0    0    -1  
$EndComp
Wire Wire Line
	6550 2150 6550 2200
Wire Wire Line
	8250 2550 8250 2500
Wire Wire Line
	8250 3150 8250 3200
Wire Wire Line
	8250 2200 8600 2200
Wire Wire Line
	9100 2150 9100 2200
Wire Wire Line
	9100 1500 9100 1550
Wire Wire Line
	8250 1500 8600 1500
Connection ~ 6550 2200
Connection ~ 6550 2500
Wire Wire Line
	7100 3850 6850 3850
Wire Wire Line
	6550 4200 6900 4200
Wire Wire Line
	7400 4150 7400 4200
$Comp
L LED:SK6805 D?
U 1 1 5E6CAB2C
P 9100 1850
AR Path="/5E103213/5E6CAB2C" Ref="D?"  Part="1" 
AR Path="/5E6CAB2C" Ref="D6"  Part="1" 
F 0 "D6" H 9150 2100 50  0000 L CNN
F 1 "SK6805" H 9100 1600 50  0000 L CNN
F 2 "resplit66:LED_SK6805_PLCC4_2.4x2.7mm_P1.3mm_BothSide" H 9150 1550 50  0001 L TNN
F 3 "https://cdn-shop.adafruit.com/product-files/3484/3484_Datasheet.pdf" H 9200 1475 50  0001 L TNN
	1    9100 1850
	1    0    0    -1  
$EndComp
Wire Wire Line
	9100 2200 9450 2200
$Comp
L LED:SK6805 D?
U 1 1 5E6CAB37
P 9100 2850
AR Path="/5E103213/5E6CAB37" Ref="D?"  Part="1" 
AR Path="/5E6CAB37" Ref="D14"  Part="1" 
F 0 "D14" H 8900 3100 50  0000 L CNN
F 1 "SK6805" H 8800 2600 50  0000 L CNN
F 2 "resplit66:LED_SK6805_PLCC4_2.4x2.7mm_P1.3mm_BothSide" H 9150 2550 50  0001 L TNN
F 3 "https://cdn-shop.adafruit.com/product-files/3484/3484_Datasheet.pdf" H 9200 2475 50  0001 L TNN
	1    9100 2850
	-1   0    0    -1  
$EndComp
Wire Wire Line
	9100 3200 9450 3200
$Comp
L LED:SK6805 D?
U 1 1 5E6CAB3E
P 9100 4850
AR Path="/5E103213/5E6CAB3E" Ref="D?"  Part="1" 
AR Path="/5E6CAB3E" Ref="D30"  Part="1" 
F 0 "D30" H 8950 5100 50  0000 L CNN
F 1 "SK6805" H 8800 4600 50  0000 L CNN
F 2 "resplit66:LED_SK6805_PLCC4_2.4x2.7mm_P1.3mm_BothSide" H 9150 4550 50  0001 L TNN
F 3 "https://cdn-shop.adafruit.com/product-files/3484/3484_Datasheet.pdf" H 9200 4475 50  0001 L TNN
	1    9100 4850
	-1   0    0    -1  
$EndComp
$Comp
L LED:SK6805 D?
U 1 1 5E6CAB45
P 8250 2850
AR Path="/5E103213/5E6CAB45" Ref="D?"  Part="1" 
AR Path="/5E6CAB45" Ref="D13"  Part="1" 
F 0 "D13" H 8050 3100 50  0000 L CNN
F 1 "SK6805" H 7950 2600 50  0000 L CNN
F 2 "resplit66:LED_SK6805_PLCC4_2.4x2.7mm_P1.3mm_BothSide" H 8300 2550 50  0001 L TNN
F 3 "https://cdn-shop.adafruit.com/product-files/3484/3484_Datasheet.pdf" H 8350 2475 50  0001 L TNN
	1    8250 2850
	-1   0    0    -1  
$EndComp
$Comp
L LED:SK6805 D?
U 1 1 5E6CAB4B
P 8250 3850
AR Path="/5E103213/5E6CAB4B" Ref="D?"  Part="1" 
AR Path="/5E6CAB4B" Ref="D21"  Part="1" 
F 0 "D21" H 8300 4100 50  0000 L CNN
F 1 "SK6805" H 8250 3600 50  0000 L CNN
F 2 "resplit66:LED_SK6805_PLCC4_2.4x2.7mm_P1.3mm_BothSide" H 8300 3550 50  0001 L TNN
F 3 "https://cdn-shop.adafruit.com/product-files/3484/3484_Datasheet.pdf" H 8350 3475 50  0001 L TNN
	1    8250 3850
	1    0    0    -1  
$EndComp
$Comp
L LED:SK6805 D?
U 1 1 5E6CAB51
P 8250 1850
AR Path="/5E103213/5E6CAB51" Ref="D?"  Part="1" 
AR Path="/5E6CAB51" Ref="D5"  Part="1" 
F 0 "D5" H 8300 2100 50  0000 L CNN
F 1 "SK6805" H 8250 1600 50  0000 L CNN
F 2 "resplit66:LED_SK6805_PLCC4_2.4x2.7mm_P1.3mm_BothSide" H 8300 1550 50  0001 L TNN
F 3 "https://cdn-shop.adafruit.com/product-files/3484/3484_Datasheet.pdf" H 8350 1475 50  0001 L TNN
	1    8250 1850
	1    0    0    -1  
$EndComp
Connection ~ 8250 2500
Connection ~ 9100 5200
Wire Wire Line
	9100 5200 9450 5200
Wire Wire Line
	9100 5150 9100 5200
$Comp
L LED:SK6805 D?
U 1 1 5E6CAB5E
P 8250 4850
AR Path="/5E103213/5E6CAB5E" Ref="D?"  Part="1" 
AR Path="/5E6CAB5E" Ref="D29"  Part="1" 
F 0 "D29" H 8100 5100 50  0000 L CNN
F 1 "SK6805" H 7950 4600 50  0000 L CNN
F 2 "resplit66:LED_SK6805_PLCC4_2.4x2.7mm_P1.3mm_BothSide" H 8300 4550 50  0001 L TNN
F 3 "https://cdn-shop.adafruit.com/product-files/3484/3484_Datasheet.pdf" H 8350 4475 50  0001 L TNN
	1    8250 4850
	-1   0    0    -1  
$EndComp
Connection ~ 8250 4500
Wire Wire Line
	8250 5200 8600 5200
Wire Wire Line
	8250 6200 8600 6200
Connection ~ 8250 5200
Wire Wire Line
	8250 5550 8250 5500
Connection ~ 8250 6200
Wire Wire Line
	7400 6200 7750 6200
Wire Wire Line
	8250 6150 8250 6200
$Comp
L LED:SK6805 D?
U 1 1 5E6CAB6D
P 7400 5850
AR Path="/5E103213/5E6CAB6D" Ref="D?"  Part="1" 
AR Path="/5E6CAB6D" Ref="D36"  Part="1" 
F 0 "D36" H 7450 6100 50  0000 L CNN
F 1 "SK6805" H 7400 5600 50  0000 L CNN
F 2 "resplit66:LED_SK6805_PLCC4_2.4x2.7mm_P1.3mm_BothSide" H 7450 5550 50  0001 L TNN
F 3 "https://cdn-shop.adafruit.com/product-files/3484/3484_Datasheet.pdf" H 7500 5475 50  0001 L TNN
	1    7400 5850
	1    0    0    -1  
$EndComp
$Comp
L LED:SK6805 D?
U 1 1 5E6CAB73
P 7400 4850
AR Path="/5E103213/5E6CAB73" Ref="D?"  Part="1" 
AR Path="/5E6CAB73" Ref="D28"  Part="1" 
F 0 "D28" H 7200 5100 50  0000 L CNN
F 1 "SK6805" H 7100 4600 50  0000 L CNN
F 2 "resplit66:LED_SK6805_PLCC4_2.4x2.7mm_P1.3mm_BothSide" H 7450 4550 50  0001 L TNN
F 3 "https://cdn-shop.adafruit.com/product-files/3484/3484_Datasheet.pdf" H 7500 4475 50  0001 L TNN
	1    7400 4850
	-1   0    0    -1  
$EndComp
Connection ~ 7400 5200
Connection ~ 7400 5500
Wire Wire Line
	7400 5150 7400 5200
Wire Wire Line
	7950 3850 7700 3850
Wire Wire Line
	7400 4200 7750 4200
Wire Wire Line
	7400 4500 7750 4500
Wire Wire Line
	8250 4550 8250 4500
Wire Wire Line
	7400 4550 7400 4500
Connection ~ 7400 4500
Wire Wire Line
	8250 5150 8250 5200
Wire Wire Line
	8800 4850 8550 4850
Wire Wire Line
	9100 4500 9100 4550
Wire Wire Line
	8250 4500 8600 4500
$Comp
L LED:SK6805 D?
U 1 1 5E6CAB86
P 6550 5850
AR Path="/5E103213/5E6CAB86" Ref="D?"  Part="1" 
AR Path="/5E6CAB86" Ref="D35"  Part="1" 
F 0 "D35" H 6600 6100 50  0000 L CNN
F 1 "SK6805" H 6550 5600 50  0000 L CNN
F 2 "resplit66:LED_SK6805_PLCC4_2.4x2.7mm_P1.3mm_BothSide" H 6600 5550 50  0001 L TNN
F 3 "https://cdn-shop.adafruit.com/product-files/3484/3484_Datasheet.pdf" H 6650 5475 50  0001 L TNN
	1    6550 5850
	1    0    0    -1  
$EndComp
$Comp
L LED:SK6805 D?
U 1 1 5E6CAB8C
P 6550 4850
AR Path="/5E103213/5E6CAB8C" Ref="D?"  Part="1" 
AR Path="/5E6CAB8C" Ref="D27"  Part="1" 
F 0 "D27" H 6400 5100 50  0000 L CNN
F 1 "SK6805" H 6250 4600 50  0000 L CNN
F 2 "resplit66:LED_SK6805_PLCC4_2.4x2.7mm_P1.3mm_BothSide" H 6600 4550 50  0001 L TNN
F 3 "https://cdn-shop.adafruit.com/product-files/3484/3484_Datasheet.pdf" H 6650 4475 50  0001 L TNN
	1    6550 4850
	-1   0    0    -1  
$EndComp
Wire Wire Line
	7950 4850 7700 4850
Wire Wire Line
	7400 5200 7750 5200
Wire Wire Line
	6550 5500 6550 5550
Wire Wire Line
	6550 5200 6900 5200
Connection ~ 6550 5500
Wire Wire Line
	7100 4850 6850 4850
Wire Wire Line
	6550 5150 6550 5200
Connection ~ 6550 5200
Wire Wire Line
	6550 4150 6550 4200
Connection ~ 6550 4200
Connection ~ 6550 4500
Wire Wire Line
	6550 4500 6900 4500
Wire Wire Line
	6550 4500 6550 4550
Wire Wire Line
	7950 1850 7700 1850
Wire Wire Line
	8250 2150 8250 2200
Wire Wire Line
	8800 1850 8550 1850
Wire Wire Line
	8250 1550 8250 1500
Connection ~ 7400 1500
Wire Wire Line
	7400 1500 7750 1500
Connection ~ 8250 2200
Connection ~ 8250 1500
$Comp
L Device:C C?
U 1 1 5E6CABAB
P 8600 5650
AR Path="/5E103213/5E6CABAB" Ref="C?"  Part="1" 
AR Path="/5E6CABAB" Ref="C37"  Part="1" 
F 0 "C37" H 8715 5695 50  0000 L CNN
F 1 ".1u" H 8715 5605 50  0000 L CNN
F 2 "resplit66:C_0603_1608Metric_BothSide" H 8638 5500 50  0001 C CNN
F 3 "~" H 8600 5650 50  0001 C CNN
	1    8600 5650
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5E6CABB8
P 3150 5850
AR Path="/5E103213/5E6CABB8" Ref="C?"  Part="1" 
AR Path="/5E6CABB8" Ref="C38"  Part="1" 
F 0 "C38" H 3265 5895 50  0000 L CNN
F 1 "470u" H 3265 5805 50  0000 L CNN
F 2 "resplit66:C_1206_3216Metric_BothSide" H 3188 5700 50  0001 C CNN
F 3 "~" H 3150 5850 50  0001 C CNN
	1    3150 5850
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5E6CABBE
P 4350 4650
AR Path="/5E103213/5E6CABBE" Ref="C?"  Part="1" 
AR Path="/5E6CABBE" Ref="C24"  Part="1" 
F 0 "C24" H 4465 4695 50  0000 L CNN
F 1 ".1u" H 4465 4605 50  0000 L CNN
F 2 "resplit66:C_0603_1608Metric_BothSide" H 4388 4500 50  0001 C CNN
F 3 "~" H 4350 4650 50  0001 C CNN
	1    4350 4650
	1    0    0    -1  
$EndComp
Text Notes 11000 700  2    50   ~ 0
See https://skyduino.wordpress.com/2014/06/08/resume-comment-cramer-une-led-ws2812b/
$Comp
L Device:C C?
U 1 1 5E6CABCB
P 6050 2650
AR Path="/5E103213/5E6CABCB" Ref="C?"  Part="1" 
AR Path="/5E6CABCB" Ref="C10"  Part="1" 
F 0 "C10" H 6165 2695 50  0000 L CNN
F 1 ".1u" H 6165 2605 50  0000 L CNN
F 2 "resplit66:C_0603_1608Metric_BothSide" H 6088 2500 50  0001 C CNN
F 3 "~" H 6050 2650 50  0001 C CNN
	1    6050 2650
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5E6CABD7
P 5200 2650
AR Path="/5E103213/5E6CABD7" Ref="C?"  Part="1" 
AR Path="/5E6CABD7" Ref="C9"  Part="1" 
F 0 "C9" H 5315 2695 50  0000 L CNN
F 1 ".1u" H 5315 2605 50  0000 L CNN
F 2 "resplit66:C_0603_1608Metric_BothSide" H 5238 2500 50  0001 C CNN
F 3 "~" H 5200 2650 50  0001 C CNN
	1    5200 2650
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5E6CABDD
P 8600 3650
AR Path="/5E103213/5E6CABDD" Ref="C?"  Part="1" 
AR Path="/5E6CABDD" Ref="C21"  Part="1" 
F 0 "C21" H 8715 3695 50  0000 L CNN
F 1 ".1u" H 8715 3605 50  0000 L CNN
F 2 "resplit66:C_0603_1608Metric_BothSide" H 8638 3500 50  0001 C CNN
F 3 "~" H 8600 3650 50  0001 C CNN
	1    8600 3650
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5E6CABE3
P 7750 5650
AR Path="/5E103213/5E6CABE3" Ref="C?"  Part="1" 
AR Path="/5E6CABE3" Ref="C36"  Part="1" 
F 0 "C36" H 7865 5695 50  0000 L CNN
F 1 ".1u" H 7865 5605 50  0000 L CNN
F 2 "resplit66:C_0603_1608Metric_BothSide" H 7788 5500 50  0001 C CNN
F 3 "~" H 7750 5650 50  0001 C CNN
	1    7750 5650
	1    0    0    -1  
$EndComp
Connection ~ 7400 2500
Wire Wire Line
	7400 2500 7750 2500
Wire Wire Line
	7400 2200 7750 2200
Connection ~ 7400 3200
Wire Wire Line
	7400 3500 7750 3500
Wire Wire Line
	7400 3200 7750 3200
Wire Wire Line
	7950 2850 7700 2850
Wire Wire Line
	8250 3550 8250 3500
Wire Wire Line
	7400 3150 7400 3200
Wire Wire Line
	7400 3550 7400 3500
Connection ~ 7400 3500
Connection ~ 5700 2500
Wire Wire Line
	5700 2550 5700 2500
Wire Wire Line
	6550 2500 6550 2550
Wire Wire Line
	5700 2500 6050 2500
Wire Wire Line
	6250 2850 6000 2850
Wire Wire Line
	6550 3200 6900 3200
Wire Wire Line
	6550 3500 6900 3500
Wire Wire Line
	7100 2850 6850 2850
Wire Wire Line
	6550 2500 6900 2500
Wire Wire Line
	7400 2550 7400 2500
Wire Wire Line
	6550 3150 6550 3200
Wire Wire Line
	5700 3200 6050 3200
Wire Wire Line
	5700 3500 6050 3500
Connection ~ 6550 3200
Wire Wire Line
	6550 3500 6550 3550
Connection ~ 6550 3500
Wire Wire Line
	5700 3550 5700 3500
Connection ~ 5700 3500
Connection ~ 5700 3200
Wire Wire Line
	5700 3150 5700 3200
Wire Wire Line
	7400 1550 7400 1500
Connection ~ 6550 1500
Wire Wire Line
	6550 1500 6900 1500
Wire Wire Line
	7100 1850 6850 1850
Wire Wire Line
	6550 1500 6550 1550
Connection ~ 7400 2200
Wire Wire Line
	6250 1850 6000 1850
Wire Wire Line
	6550 2200 6900 2200
Wire Wire Line
	7400 2150 7400 2200
Wire Wire Line
	5700 1550 5700 1500
Wire Wire Line
	5700 1500 6050 1500
Wire Wire Line
	5700 2150 5700 2200
Connection ~ 5700 2200
Wire Wire Line
	5700 2200 6050 2200
Connection ~ 5700 1500
Connection ~ 4850 1500
Wire Wire Line
	4850 1500 4850 1550
Wire Wire Line
	4850 1500 5200 1500
Wire Wire Line
	5400 1850 5150 1850
Wire Wire Line
	4850 2200 5200 2200
Connection ~ 4850 2200
Wire Wire Line
	4850 2150 4850 2200
Connection ~ 6550 6200
Wire Wire Line
	7400 6150 7400 6200
Wire Wire Line
	7100 5850 6850 5850
Wire Wire Line
	6550 6150 6550 6200
Wire Wire Line
	7400 6200 6900 6200
Wire Wire Line
	7950 5850 7700 5850
Connection ~ 7400 6200
Wire Wire Line
	7400 5500 7750 5500
$Comp
L LED:SK6805 D?
U 1 1 5E6CAC2D
P 5700 5850
AR Path="/5E103213/5E6CAC2D" Ref="D?"  Part="1" 
AR Path="/5E6CAC2D" Ref="D34"  Part="1" 
F 0 "D34" H 5750 6100 50  0000 L CNN
F 1 "SK6805" H 5700 5600 50  0000 L CNN
F 2 "resplit66:LED_SK6805_PLCC4_2.4x2.7mm_P1.3mm_BothSide" H 5750 5550 50  0001 L TNN
F 3 "https://cdn-shop.adafruit.com/product-files/3484/3484_Datasheet.pdf" H 5800 5475 50  0001 L TNN
	1    5700 5850
	1    0    0    -1  
$EndComp
Wire Wire Line
	7400 5550 7400 5500
Wire Wire Line
	6550 5500 6900 5500
Wire Wire Line
	6250 4850 6000 4850
Wire Wire Line
	5700 5500 6050 5500
Wire Wire Line
	5700 5550 5700 5500
Connection ~ 5700 4500
Connection ~ 5700 4200
Wire Wire Line
	5700 4150 5700 4200
Wire Wire Line
	5700 4200 6050 4200
Wire Wire Line
	5700 4550 5700 4500
Wire Wire Line
	5700 4500 6050 4500
Wire Wire Line
	6250 3850 6000 3850
Connection ~ 5700 5200
Wire Wire Line
	5700 5150 5700 5200
Connection ~ 5700 5500
Wire Wire Line
	5700 5200 6050 5200
Wire Wire Line
	6550 6200 6050 6200
Wire Wire Line
	6250 5850 6000 5850
Wire Wire Line
	5700 6150 5700 6200
Connection ~ 5700 6200
$Comp
L LED:SK6805 D?
U 1 1 5E6CAC47
P 5700 1850
AR Path="/5E103213/5E6CAC47" Ref="D?"  Part="1" 
AR Path="/5E6CAC47" Ref="D2"  Part="1" 
F 0 "D2" H 5750 2100 50  0000 L CNN
F 1 "SK6805" H 5700 1600 50  0000 L CNN
F 2 "resplit66:LED_SK6805_PLCC4_2.4x2.7mm_P1.3mm_BothSide" H 5750 1550 50  0001 L TNN
F 3 "https://cdn-shop.adafruit.com/product-files/3484/3484_Datasheet.pdf" H 5800 1475 50  0001 L TNN
	1    5700 1850
	1    0    0    -1  
$EndComp
$Comp
L LED:SK6805 D?
U 1 1 5E6CAC4D
P 5700 3850
AR Path="/5E103213/5E6CAC4D" Ref="D?"  Part="1" 
AR Path="/5E6CAC4D" Ref="D18"  Part="1" 
F 0 "D18" H 5750 4100 50  0000 L CNN
F 1 "SK6805" H 5700 3600 50  0000 L CNN
F 2 "resplit66:LED_SK6805_PLCC4_2.4x2.7mm_P1.3mm_BothSide" H 5750 3550 50  0001 L TNN
F 3 "https://cdn-shop.adafruit.com/product-files/3484/3484_Datasheet.pdf" H 5800 3475 50  0001 L TNN
	1    5700 3850
	1    0    0    -1  
$EndComp
$Comp
L LED:SK6805 D?
U 1 1 5E6CAC53
P 5700 2850
AR Path="/5E103213/5E6CAC53" Ref="D?"  Part="1" 
AR Path="/5E6CAC53" Ref="D10"  Part="1" 
F 0 "D10" H 5500 3100 50  0000 L CNN
F 1 "SK6805" H 5400 2600 50  0000 L CNN
F 2 "resplit66:LED_SK6805_PLCC4_2.4x2.7mm_P1.3mm_BothSide" H 5750 2550 50  0001 L TNN
F 3 "https://cdn-shop.adafruit.com/product-files/3484/3484_Datasheet.pdf" H 5800 2475 50  0001 L TNN
	1    5700 2850
	-1   0    0    -1  
$EndComp
$Comp
L LED:SK6805 D?
U 1 1 5E6CAC59
P 4850 2850
AR Path="/5E103213/5E6CAC59" Ref="D?"  Part="1" 
AR Path="/5E6CAC59" Ref="D9"  Part="1" 
F 0 "D9" H 4650 3100 50  0000 L CNN
F 1 "SK6805" H 4550 2600 50  0000 L CNN
F 2 "resplit66:LED_SK6805_PLCC4_2.4x2.7mm_P1.3mm_BothSide" H 4900 2550 50  0001 L TNN
F 3 "https://cdn-shop.adafruit.com/product-files/3484/3484_Datasheet.pdf" H 4950 2475 50  0001 L TNN
	1    4850 2850
	-1   0    0    -1  
$EndComp
$Comp
L LED:SK6805 D?
U 1 1 5E6CAC5F
P 4850 1850
AR Path="/5E103213/5E6CAC5F" Ref="D?"  Part="1" 
AR Path="/5E6CAC5F" Ref="D1"  Part="1" 
F 0 "D1" H 4900 2100 50  0000 L CNN
F 1 "SK6805" H 4850 1600 50  0000 L CNN
F 2 "resplit66:LED_SK6805_PLCC4_2.4x2.7mm_P1.3mm_BothSide" H 4900 1550 50  0001 L TNN
F 3 "https://cdn-shop.adafruit.com/product-files/3484/3484_Datasheet.pdf" H 4950 1475 50  0001 L TNN
	1    4850 1850
	1    0    0    -1  
$EndComp
$Comp
L LED:SK6805 D?
U 1 1 5E6CAC65
P 4850 3850
AR Path="/5E103213/5E6CAC65" Ref="D?"  Part="1" 
AR Path="/5E6CAC65" Ref="D17"  Part="1" 
F 0 "D17" H 4900 4100 50  0000 L CNN
F 1 "SK6805" H 4850 3600 50  0000 L CNN
F 2 "resplit66:LED_SK6805_PLCC4_2.4x2.7mm_P1.3mm_BothSide" H 4900 3550 50  0001 L TNN
F 3 "https://cdn-shop.adafruit.com/product-files/3484/3484_Datasheet.pdf" H 4950 3475 50  0001 L TNN
	1    4850 3850
	1    0    0    -1  
$EndComp
$Comp
L LED:SK6805 D?
U 1 1 5E6CAC6B
P 5700 4850
AR Path="/5E103213/5E6CAC6B" Ref="D?"  Part="1" 
AR Path="/5E6CAC6B" Ref="D26"  Part="1" 
F 0 "D26" H 5550 5100 50  0000 L CNN
F 1 "SK6805" H 5400 4600 50  0000 L CNN
F 2 "resplit66:LED_SK6805_PLCC4_2.4x2.7mm_P1.3mm_BothSide" H 5750 4550 50  0001 L TNN
F 3 "https://cdn-shop.adafruit.com/product-files/3484/3484_Datasheet.pdf" H 5800 4475 50  0001 L TNN
	1    5700 4850
	-1   0    0    -1  
$EndComp
$Comp
L LED:SK6805 D?
U 1 1 5E6CAC71
P 4850 5850
AR Path="/5E103213/5E6CAC71" Ref="D?"  Part="1" 
AR Path="/5E6CAC71" Ref="D33"  Part="1" 
F 0 "D33" H 4900 6100 50  0000 L CNN
F 1 "SK6805" H 4850 5600 50  0000 L CNN
F 2 "resplit66:LED_SK6805_PLCC4_2.4x2.7mm_P1.3mm_BothSide" H 4900 5550 50  0001 L TNN
F 3 "https://cdn-shop.adafruit.com/product-files/3484/3484_Datasheet.pdf" H 4950 5475 50  0001 L TNN
	1    4850 5850
	1    0    0    -1  
$EndComp
$Comp
L LED:SK6805 D?
U 1 1 5E6CAC77
P 4000 5850
AR Path="/5E103213/5E6CAC77" Ref="D?"  Part="1" 
AR Path="/5E6CAC77" Ref="D32"  Part="1" 
F 0 "D32" H 4050 6100 50  0000 L CNN
F 1 "SK6805" H 4000 5600 50  0000 L CNN
F 2 "resplit66:LED_SK6805_PLCC4_2.4x2.7mm_P1.3mm_BothSide" H 4050 5550 50  0001 L TNN
F 3 "https://cdn-shop.adafruit.com/product-files/3484/3484_Datasheet.pdf" H 4100 5475 50  0001 L TNN
	1    4000 5850
	1    0    0    -1  
$EndComp
Wire Wire Line
	4850 6200 5200 6200
Wire Wire Line
	5400 5850 5150 5850
Wire Wire Line
	4850 5500 4850 5550
Wire Wire Line
	4000 5500 4350 5500
Wire Wire Line
	4000 5550 4000 5500
Connection ~ 4850 5500
$Comp
L LED:SK6805 D?
U 1 1 5E6CAC84
P 4850 4850
AR Path="/5E103213/5E6CAC84" Ref="D?"  Part="1" 
AR Path="/5E6CAC84" Ref="D25"  Part="1" 
F 0 "D25" H 4650 5100 50  0000 L CNN
F 1 "SK6805" H 4550 4600 50  0000 L CNN
F 2 "resplit66:LED_SK6805_PLCC4_2.4x2.7mm_P1.3mm_BothSide" H 4900 4550 50  0001 L TNN
F 3 "https://cdn-shop.adafruit.com/product-files/3484/3484_Datasheet.pdf" H 4950 4475 50  0001 L TNN
	1    4850 4850
	-1   0    0    -1  
$EndComp
Wire Wire Line
	4850 5200 5200 5200
Connection ~ 4850 5200
Wire Wire Line
	4850 5150 4850 5200
Wire Wire Line
	4850 5500 5200 5500
Wire Wire Line
	4550 5850 4300 5850
Wire Wire Line
	4850 6150 4850 6200
Connection ~ 4850 6200
Wire Wire Line
	4850 6200 4350 6200
Wire Wire Line
	4000 6150 4000 6200
Connection ~ 4000 5500
Wire Wire Line
	3150 2500 4000 2500
$Comp
L LED:SK6805 D?
U 1 1 5E6CAC97
P 4000 2850
AR Path="/5E103213/5E6CAC97" Ref="D?"  Part="1" 
AR Path="/5E6CAC97" Ref="D8"  Part="1" 
F 0 "D8" H 3800 3100 50  0000 L CNN
F 1 "SK6805" H 3700 2600 50  0000 L CNN
F 2 "resplit66:LED_SK6805_PLCC4_2.4x2.7mm_P1.3mm_BothSide" H 4050 2550 50  0001 L TNN
F 3 "https://cdn-shop.adafruit.com/product-files/3484/3484_Datasheet.pdf" H 4100 2475 50  0001 L TNN
	1    4000 2850
	-1   0    0    -1  
$EndComp
$Comp
L LED:SK6805 D?
U 1 1 5E6CAC9D
P 4000 4850
AR Path="/5E103213/5E6CAC9D" Ref="D?"  Part="1" 
AR Path="/5E6CAC9D" Ref="D24"  Part="1" 
F 0 "D24" H 3800 5100 50  0000 L CNN
F 1 "SK6805" H 3700 4600 50  0000 L CNN
F 2 "resplit66:LED_SK6805_PLCC4_2.4x2.7mm_P1.3mm_BothSide" H 4050 4550 50  0001 L TNN
F 3 "https://cdn-shop.adafruit.com/product-files/3484/3484_Datasheet.pdf" H 4100 4475 50  0001 L TNN
	1    4000 4850
	-1   0    0    -1  
$EndComp
Connection ~ 4850 4500
Wire Wire Line
	4850 4500 5200 4500
Connection ~ 4000 4500
Connection ~ 4850 3500
Wire Wire Line
	4000 3500 4350 3500
$Comp
L LED:SK6805 D?
U 1 1 5E6CACA8
P 4000 1850
AR Path="/5E103213/5E6CACA8" Ref="D?"  Part="1" 
AR Path="/5E6CACA8" Ref="D0"  Part="1" 
F 0 "D0" H 4050 2100 50  0000 L CNN
F 1 "SK6805" H 4000 1600 50  0000 L CNN
F 2 "resplit66:LED_SK6805_PLCC4_2.4x2.7mm_P1.3mm_BothSide" H 4050 1550 50  0001 L TNN
F 3 "https://cdn-shop.adafruit.com/product-files/3484/3484_Datasheet.pdf" H 4100 1475 50  0001 L TNN
	1    4000 1850
	1    0    0    -1  
$EndComp
Wire Wire Line
	4850 2500 5200 2500
Wire Wire Line
	4000 2500 4350 2500
Wire Wire Line
	4000 2200 4350 2200
Wire Wire Line
	4550 1850 4300 1850
Wire Wire Line
	4000 2150 4000 2200
Wire Wire Line
	4850 3150 4850 3200
Wire Wire Line
	4000 3150 4000 3200
Wire Wire Line
	4550 2850 4300 2850
Connection ~ 4850 2500
Wire Wire Line
	4000 2550 4000 2500
Wire Wire Line
	4850 2500 4850 2550
Wire Wire Line
	4000 3200 4350 3200
Connection ~ 4000 2500
Wire Wire Line
	4550 4850 4300 4850
Wire Wire Line
	5400 4850 5150 4850
Wire Wire Line
	4850 4500 4850 4550
Wire Wire Line
	4000 4550 4000 4500
Wire Wire Line
	4000 4500 4350 4500
$Comp
L LED:SK6805 D?
U 1 1 5E6CACC0
P 4000 3850
AR Path="/5E103213/5E6CACC0" Ref="D?"  Part="1" 
AR Path="/5E6CACC0" Ref="D16"  Part="1" 
F 0 "D16" H 4050 4100 50  0000 L CNN
F 1 "SK6805" H 4000 3600 50  0000 L CNN
F 2 "resplit66:LED_SK6805_PLCC4_2.4x2.7mm_P1.3mm_BothSide" H 4050 3550 50  0001 L TNN
F 3 "https://cdn-shop.adafruit.com/product-files/3484/3484_Datasheet.pdf" H 4100 3475 50  0001 L TNN
	1    4000 3850
	1    0    0    -1  
$EndComp
Connection ~ 4850 4200
Wire Wire Line
	5400 3850 5150 3850
Wire Wire Line
	4850 4200 5200 4200
Wire Wire Line
	4850 4150 4850 4200
Wire Wire Line
	4000 4150 4000 4200
Wire Wire Line
	4000 4200 4350 4200
Wire Wire Line
	4850 3500 5200 3500
Wire Wire Line
	4850 3200 5200 3200
Connection ~ 4850 3200
Wire Wire Line
	5400 2850 5150 2850
Wire Wire Line
	4000 3550 4000 3500
Wire Wire Line
	4850 3500 4850 3550
Connection ~ 4000 3500
Wire Wire Line
	4550 3850 4300 3850
Wire Wire Line
	4000 5200 4350 5200
Wire Wire Line
	4000 5150 4000 5200
Wire Wire Line
	3150 5500 4000 5500
Wire Wire Line
	3150 4500 4000 4500
Wire Wire Line
	3150 3500 4000 3500
$Comp
L Device:R R?
U 1 1 5E6CACF0
P 1950 6500
AR Path="/5E103213/5E6CACF0" Ref="R?"  Part="1" 
AR Path="/5E6CACF0" Ref="R1"  Part="1" 
F 0 "R1" V 2157 6500 50  0000 C CNN
F 1 "4.7k" V 2066 6500 50  0000 C CNN
F 2 "resplit66:R_0603_1608Metric_BothSide" V 1880 6500 50  0001 C CNN
F 3 "~" H 1950 6500 50  0001 C CNN
	1    1950 6500
	0    1    -1   0   
$EndComp
$Comp
L Device:Q_NMOS_GSD Q?
U 1 1 5E6CACFC
P 2400 6500
AR Path="/5E103213/5E6CACFC" Ref="Q?"  Part="1" 
AR Path="/5E6CACFC" Ref="Q1"  Part="1" 
F 0 "Q1" H 2604 6546 50  0000 L CNN
F 1 "DMN63D8L" H 2604 6455 50  0000 L CNN
F 2 "resplit66:SOT-23_BothSide" H 2600 6600 50  0001 C CNN
F 3 "~" H 2400 6500 50  0001 C CNN
	1    2400 6500
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5E6CAD03
P 2500 6800
AR Path="/5E103213/5E6CAD03" Ref="#PWR?"  Part="1" 
AR Path="/5E6CAD03" Ref="#PWR04"  Part="1" 
F 0 "#PWR04" H 2500 6550 50  0001 C CNN
F 1 "GND" H 2505 6627 50  0000 C CNN
F 2 "" H 2500 6800 50  0001 C CNN
F 3 "" H 2500 6800 50  0001 C CNN
	1    2500 6800
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5E6CAD2F
P 2250 2000
AR Path="/5E103213/5E6CAD2F" Ref="#PWR?"  Part="1" 
AR Path="/5E6CAD2F" Ref="#PWR03"  Part="1" 
F 0 "#PWR03" H 2250 1750 50  0001 C CNN
F 1 "GND" H 2255 1827 50  0000 C CNN
F 2 "" H 2250 2000 50  0001 C CNN
F 3 "" H 2250 2000 50  0001 C CNN
	1    2250 2000
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x05_Odd_Even J?
U 1 1 5E6CAD5C
P 1850 1700
AR Path="/5E103213/5E6CAD5C" Ref="J?"  Part="1" 
AR Path="/5E6CAD5C" Ref="J1"  Part="1" 
F 0 "J1" H 1900 2117 50  0000 C CNN
F 1 "RGB_Oled" H 1900 2026 50  0000 C CNN
F 2 "Connector_PinHeader_1.27mm:PinHeader_2x05_P1.27mm_Vertical" H 1850 1700 50  0001 C CNN
F 3 "~" H 1850 1700 50  0001 C CNN
	1    1850 1700
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 5E6CAD64
P 2500 1250
AR Path="/5E103213/5E6CAD64" Ref="#PWR?"  Part="1" 
AR Path="/5E6CAD64" Ref="#PWR02"  Part="1" 
F 0 "#PWR02" H 2500 1100 50  0001 C CNN
F 1 "+5V" H 2515 1423 50  0000 C CNN
F 2 "" H 2500 1250 50  0001 C CNN
F 3 "" H 2500 1250 50  0001 C CNN
	1    2500 1250
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR?
U 1 1 5E6CAD6B
P 1300 1250
AR Path="/5E103213/5E6CAD6B" Ref="#PWR?"  Part="1" 
AR Path="/5E6CAD6B" Ref="#PWR01"  Part="1" 
F 0 "#PWR01" H 1300 1100 50  0001 C CNN
F 1 "+3.3V" H 1315 1423 50  0000 C CNN
F 2 "" H 1300 1250 50  0001 C CNN
F 3 "" H 1300 1250 50  0001 C CNN
	1    1300 1250
	1    0    0    -1  
$EndComp
Wire Wire Line
	1300 1500 1300 1250
$Comp
L Device:C C?
U 1 1 5E6CAD83
P 6900 3650
AR Path="/5E103213/5E6CAD83" Ref="C?"  Part="1" 
AR Path="/5E6CAD83" Ref="C19"  Part="1" 
F 0 "C19" H 7015 3695 50  0000 L CNN
F 1 ".1u" H 7015 3605 50  0000 L CNN
F 2 "resplit66:C_0603_1608Metric_BothSide" H 6938 3500 50  0001 C CNN
F 3 "~" H 6900 3650 50  0001 C CNN
	1    6900 3650
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5E6CAD89
P 9450 1650
AR Path="/5E103213/5E6CAD89" Ref="C?"  Part="1" 
AR Path="/5E6CAD89" Ref="C6"  Part="1" 
F 0 "C6" H 9565 1695 50  0000 L CNN
F 1 ".1u" H 9565 1605 50  0000 L CNN
F 2 "resplit66:C_0603_1608Metric_BothSide" H 9488 1500 50  0001 C CNN
F 3 "~" H 9450 1650 50  0001 C CNN
	1    9450 1650
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5E6CAD8F
P 6050 5650
AR Path="/5E103213/5E6CAD8F" Ref="C?"  Part="1" 
AR Path="/5E6CAD8F" Ref="C34"  Part="1" 
F 0 "C34" H 6165 5695 50  0000 L CNN
F 1 ".1u" H 6165 5605 50  0000 L CNN
F 2 "resplit66:C_0603_1608Metric_BothSide" H 6088 5500 50  0001 C CNN
F 3 "~" H 6050 5650 50  0001 C CNN
	1    6050 5650
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5E6CAD9B
P 7750 3650
AR Path="/5E103213/5E6CAD9B" Ref="C?"  Part="1" 
AR Path="/5E6CAD9B" Ref="C20"  Part="1" 
F 0 "C20" H 7865 3695 50  0000 L CNN
F 1 ".1u" H 7865 3605 50  0000 L CNN
F 2 "resplit66:C_0603_1608Metric_BothSide" H 7788 3500 50  0001 C CNN
F 3 "~" H 7750 3650 50  0001 C CNN
	1    7750 3650
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5E6CADA1
P 4350 2650
AR Path="/5E103213/5E6CADA1" Ref="C?"  Part="1" 
AR Path="/5E6CADA1" Ref="C8"  Part="1" 
F 0 "C8" H 4465 2695 50  0000 L CNN
F 1 ".1u" H 4465 2605 50  0000 L CNN
F 2 "resplit66:C_0603_1608Metric_BothSide" H 4388 2500 50  0001 C CNN
F 3 "~" H 4350 2650 50  0001 C CNN
	1    4350 2650
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5E6CADA7
P 6900 5650
AR Path="/5E103213/5E6CADA7" Ref="C?"  Part="1" 
AR Path="/5E6CADA7" Ref="C35"  Part="1" 
F 0 "C35" H 7015 5695 50  0000 L CNN
F 1 ".1u" H 7015 5605 50  0000 L CNN
F 2 "resplit66:C_0603_1608Metric_BothSide" H 6938 5500 50  0001 C CNN
F 3 "~" H 6900 5650 50  0001 C CNN
	1    6900 5650
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5E6CADBF
P 8600 1650
AR Path="/5E103213/5E6CADBF" Ref="C?"  Part="1" 
AR Path="/5E6CADBF" Ref="C5"  Part="1" 
F 0 "C5" H 8715 1695 50  0000 L CNN
F 1 ".1u" H 8715 1605 50  0000 L CNN
F 2 "resplit66:C_0603_1608Metric_BothSide" H 8638 1500 50  0001 C CNN
F 3 "~" H 8600 1650 50  0001 C CNN
	1    8600 1650
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5E6CADC7
P 6050 3650
AR Path="/5E103213/5E6CADC7" Ref="C?"  Part="1" 
AR Path="/5E6CADC7" Ref="C18"  Part="1" 
F 0 "C18" H 6165 3695 50  0000 L CNN
F 1 ".1u" H 6165 3605 50  0000 L CNN
F 2 "resplit66:C_0603_1608Metric_BothSide" H 6088 3500 50  0001 C CNN
F 3 "~" H 6050 3650 50  0001 C CNN
	1    6050 3650
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5E6CADCD
P 5200 5650
AR Path="/5E103213/5E6CADCD" Ref="C?"  Part="1" 
AR Path="/5E6CADCD" Ref="C33"  Part="1" 
F 0 "C33" H 5315 5695 50  0000 L CNN
F 1 ".1u" H 5315 5605 50  0000 L CNN
F 2 "resplit66:C_0603_1608Metric_BothSide" H 5238 5500 50  0001 C CNN
F 3 "~" H 5200 5650 50  0001 C CNN
	1    5200 5650
	1    0    0    -1  
$EndComp
Wire Wire Line
	3150 1500 4000 1500
Connection ~ 4000 1500
Wire Wire Line
	4000 1550 4000 1500
$Comp
L Device:C C?
U 1 1 5E6CADF0
P 5200 1650
AR Path="/5E103213/5E6CADF0" Ref="C?"  Part="1" 
AR Path="/5E6CADF0" Ref="C1"  Part="1" 
F 0 "C1" H 5315 1695 50  0000 L CNN
F 1 ".1u" H 5315 1605 50  0000 L CNN
F 2 "resplit66:C_0603_1608Metric_BothSide" H 5238 1500 50  0001 C CNN
F 3 "~" H 5200 1650 50  0001 C CNN
	1    5200 1650
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5E6CADF6
P 8600 2650
AR Path="/5E103213/5E6CADF6" Ref="C?"  Part="1" 
AR Path="/5E6CADF6" Ref="C13"  Part="1" 
F 0 "C13" H 8715 2695 50  0000 L CNN
F 1 ".1u" H 8715 2605 50  0000 L CNN
F 2 "resplit66:C_0603_1608Metric_BothSide" H 8638 2500 50  0001 C CNN
F 3 "~" H 8600 2650 50  0001 C CNN
	1    8600 2650
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5E6CAE00
P 4350 1650
AR Path="/5E103213/5E6CAE00" Ref="C?"  Part="1" 
AR Path="/5E6CAE00" Ref="C0"  Part="1" 
F 0 "C0" H 4465 1695 50  0000 L CNN
F 1 ".1u" H 4465 1605 50  0000 L CNN
F 2 "resplit66:C_0603_1608Metric_BothSide" H 4388 1500 50  0001 C CNN
F 3 "~" H 4350 1650 50  0001 C CNN
	1    4350 1650
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5E6CAE07
P 7750 2650
AR Path="/5E103213/5E6CAE07" Ref="C?"  Part="1" 
AR Path="/5E6CAE07" Ref="C12"  Part="1" 
F 0 "C12" H 7865 2695 50  0000 L CNN
F 1 ".1u" H 7865 2605 50  0000 L CNN
F 2 "resplit66:C_0603_1608Metric_BothSide" H 7788 2500 50  0001 C CNN
F 3 "~" H 7750 2650 50  0001 C CNN
	1    7750 2650
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5E6CAE0D
P 6900 4650
AR Path="/5E103213/5E6CAE0D" Ref="C?"  Part="1" 
AR Path="/5E6CAE0D" Ref="C27"  Part="1" 
F 0 "C27" H 7015 4695 50  0000 L CNN
F 1 ".1u" H 7015 4605 50  0000 L CNN
F 2 "resplit66:C_0603_1608Metric_BothSide" H 6938 4500 50  0001 C CNN
F 3 "~" H 6900 4650 50  0001 C CNN
	1    6900 4650
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5E6CAE14
P 5200 4650
AR Path="/5E103213/5E6CAE14" Ref="C?"  Part="1" 
AR Path="/5E6CAE14" Ref="C25"  Part="1" 
F 0 "C25" H 5315 4695 50  0000 L CNN
F 1 ".1u" H 5315 4605 50  0000 L CNN
F 2 "resplit66:C_0603_1608Metric_BothSide" H 5238 4500 50  0001 C CNN
F 3 "~" H 5200 4650 50  0001 C CNN
	1    5200 4650
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5E6CAE1A
P 6900 2650
AR Path="/5E103213/5E6CAE1A" Ref="C?"  Part="1" 
AR Path="/5E6CAE1A" Ref="C11"  Part="1" 
F 0 "C11" H 7015 2695 50  0000 L CNN
F 1 ".1u" H 7015 2605 50  0000 L CNN
F 2 "resplit66:C_0603_1608Metric_BothSide" H 6938 2500 50  0001 C CNN
F 3 "~" H 6900 2650 50  0001 C CNN
	1    6900 2650
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5E6CAE26
P 6050 4650
AR Path="/5E103213/5E6CAE26" Ref="C?"  Part="1" 
AR Path="/5E6CAE26" Ref="C26"  Part="1" 
F 0 "C26" H 6165 4695 50  0000 L CNN
F 1 ".1u" H 6165 4605 50  0000 L CNN
F 2 "resplit66:C_0603_1608Metric_BothSide" H 6088 4500 50  0001 C CNN
F 3 "~" H 6050 4650 50  0001 C CNN
	1    6050 4650
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5E6CAE32
P 7750 4650
AR Path="/5E103213/5E6CAE32" Ref="C?"  Part="1" 
AR Path="/5E6CAE32" Ref="C28"  Part="1" 
F 0 "C28" H 7865 4695 50  0000 L CNN
F 1 ".1u" H 7865 4605 50  0000 L CNN
F 2 "resplit66:C_0603_1608Metric_BothSide" H 7788 4500 50  0001 C CNN
F 3 "~" H 7750 4650 50  0001 C CNN
	1    7750 4650
	1    0    0    -1  
$EndComp
Wire Wire Line
	4000 1500 4350 1500
$Comp
L Device:C C?
U 1 1 5E6CAE3C
P 8600 4650
AR Path="/5E103213/5E6CAE3C" Ref="C?"  Part="1" 
AR Path="/5E6CAE3C" Ref="C29"  Part="1" 
F 0 "C29" H 8715 4695 50  0000 L CNN
F 1 ".1u" H 8715 4605 50  0000 L CNN
F 2 "resplit66:C_0603_1608Metric_BothSide" H 8638 4500 50  0001 C CNN
F 3 "~" H 8600 4650 50  0001 C CNN
	1    8600 4650
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5E6CAE42
P 9450 2650
AR Path="/5E103213/5E6CAE42" Ref="C?"  Part="1" 
AR Path="/5E6CAE42" Ref="C14"  Part="1" 
F 0 "C14" H 9565 2695 50  0000 L CNN
F 1 ".1u" H 9565 2605 50  0000 L CNN
F 2 "resplit66:C_0603_1608Metric_BothSide" H 9488 2500 50  0001 C CNN
F 3 "~" H 9450 2650 50  0001 C CNN
	1    9450 2650
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5E6CAE51
P 6050 1650
AR Path="/5E103213/5E6CAE51" Ref="C?"  Part="1" 
AR Path="/5E6CAE51" Ref="C2"  Part="1" 
F 0 "C2" H 6165 1695 50  0000 L CNN
F 1 ".1u" H 6165 1605 50  0000 L CNN
F 2 "resplit66:C_0603_1608Metric_BothSide" H 6088 1500 50  0001 C CNN
F 3 "~" H 6050 1650 50  0001 C CNN
	1    6050 1650
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5E6CAE57
P 7750 1650
AR Path="/5E103213/5E6CAE57" Ref="C?"  Part="1" 
AR Path="/5E6CAE57" Ref="C4"  Part="1" 
F 0 "C4" H 7865 1695 50  0000 L CNN
F 1 ".1u" H 7865 1605 50  0000 L CNN
F 2 "resplit66:C_0603_1608Metric_BothSide" H 7788 1500 50  0001 C CNN
F 3 "~" H 7750 1650 50  0001 C CNN
	1    7750 1650
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5E6CAE5D
P 5200 3650
AR Path="/5E103213/5E6CAE5D" Ref="C?"  Part="1" 
AR Path="/5E6CAE5D" Ref="C17"  Part="1" 
F 0 "C17" H 5315 3695 50  0000 L CNN
F 1 ".1u" H 5315 3605 50  0000 L CNN
F 2 "resplit66:C_0603_1608Metric_BothSide" H 5238 3500 50  0001 C CNN
F 3 "~" H 5200 3650 50  0001 C CNN
	1    5200 3650
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5E6CAE6C
P 4350 5650
AR Path="/5E103213/5E6CAE6C" Ref="C?"  Part="1" 
AR Path="/5E6CAE6C" Ref="C32"  Part="1" 
F 0 "C32" H 4465 5695 50  0000 L CNN
F 1 ".1u" H 4465 5605 50  0000 L CNN
F 2 "resplit66:C_0603_1608Metric_BothSide" H 4388 5500 50  0001 C CNN
F 3 "~" H 4350 5650 50  0001 C CNN
	1    4350 5650
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5E6CAE7A
P 6900 1650
AR Path="/5E103213/5E6CAE7A" Ref="C?"  Part="1" 
AR Path="/5E6CAE7A" Ref="C3"  Part="1" 
F 0 "C3" H 7015 1695 50  0000 L CNN
F 1 ".1u" H 7015 1605 50  0000 L CNN
F 2 "resplit66:C_0603_1608Metric_BothSide" H 6938 1500 50  0001 C CNN
F 3 "~" H 6900 1650 50  0001 C CNN
	1    6900 1650
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5E6CAE81
P 4350 3650
AR Path="/5E103213/5E6CAE81" Ref="C?"  Part="1" 
AR Path="/5E6CAE81" Ref="C16"  Part="1" 
F 0 "C16" H 4465 3695 50  0000 L CNN
F 1 ".1u" H 4465 3605 50  0000 L CNN
F 2 "resplit66:C_0603_1608Metric_BothSide" H 4388 3500 50  0001 C CNN
F 3 "~" H 4350 3650 50  0001 C CNN
	1    4350 3650
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5E6CAE87
P 9450 4650
AR Path="/5E103213/5E6CAE87" Ref="C?"  Part="1" 
AR Path="/5E6CAE87" Ref="C30"  Part="1" 
F 0 "C30" H 9565 4695 50  0000 L CNN
F 1 ".1u" H 9565 4605 50  0000 L CNN
F 2 "resplit66:C_0603_1608Metric_BothSide" H 9488 4500 50  0001 C CNN
F 3 "~" H 9450 4650 50  0001 C CNN
	1    9450 4650
	1    0    0    -1  
$EndComp
$Comp
L LED:SK6805 D?
U 1 1 5E6CAE96
P 8250 5850
AR Path="/5E103213/5E6CAE96" Ref="D?"  Part="1" 
AR Path="/5E6CAE96" Ref="D37"  Part="1" 
F 0 "D37" H 8300 6100 50  0000 L CNN
F 1 "SK6805" H 8250 5600 50  0000 L CNN
F 2 "resplit66:LED_SK6805_PLCC4_2.4x2.7mm_P1.3mm_BothSide" H 8300 5550 50  0001 L TNN
F 3 "https://cdn-shop.adafruit.com/product-files/3484/3484_Datasheet.pdf" H 8350 5475 50  0001 L TNN
	1    8250 5850
	1    0    0    -1  
$EndComp
NoConn ~ 8550 5850
Text Notes 3350 7000 0    50   ~ 0
Not all leds need to be places. But if not installed, a small wrap must be installed \nto shorten DIN and DOUT. So you can select how many leds you want.
Text Notes 3350 7400 0    50   ~ 0
LEDS require 5V power with 100n decoupling. So MOSFET is mandatory to ensure\nlow Vdrop. LEDS require 3.4V min (VIH) and 1.6V max (VIL) at first LED DIN pin.\nSo a classic NPN with Vdrop=0.6V is good enough but a driver is used.\nR1 required to limit current due to high capacitance of MOSFET gate.
Text Notes 3350 7550 0    50   ~ 0
Since MISO isn't required for RGB leds, it is used for FULL LEDS power on/off.\n
NoConn ~ 2150 1700
NoConn ~ 2150 1800
NoConn ~ 1650 1900
Wire Wire Line
	1300 1500 1650 1500
$Comp
L Driver_FET:MCP1416 U1
U 1 1 5FEE1112
P 2500 2850
F 0 "U1" H 2700 3100 50  0000 L CNN
F 1 "MCP1416" H 2550 2600 50  0000 L CNN
F 2 "resplit66:SOT-23-5_BothSide" H 2500 2450 50  0001 C CIN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/20002092F.pdf" H 2300 3100 50  0001 C CNN
	1    2500 2850
	1    0    0    -1  
$EndComp
Connection ~ 2500 1500
Wire Wire Line
	2150 1500 2500 1500
Wire Wire Line
	2500 1500 2500 1250
Wire Wire Line
	2150 1900 2250 1900
Wire Wire Line
	2250 1900 2250 2000
Wire Wire Line
	1650 1800 1550 1800
Wire Wire Line
	1550 2850 2200 2850
Connection ~ 4350 1500
Wire Wire Line
	4350 1500 4850 1500
Connection ~ 5200 1500
Wire Wire Line
	5200 1500 5700 1500
Connection ~ 6050 1500
Wire Wire Line
	6050 1500 6550 1500
Connection ~ 6900 1500
Wire Wire Line
	6900 1500 7400 1500
Connection ~ 7750 1500
Wire Wire Line
	7750 1500 8250 1500
Connection ~ 8600 1500
Wire Wire Line
	8600 1500 9100 1500
Wire Wire Line
	9100 1500 9450 1500
Connection ~ 9100 1500
Wire Wire Line
	9450 1800 9450 2200
Connection ~ 9450 2200
Wire Wire Line
	9450 2200 10000 2200
Wire Wire Line
	8600 1800 8600 2200
Connection ~ 8600 2200
Wire Wire Line
	8600 2200 9100 2200
Wire Wire Line
	7750 1800 7750 2200
Connection ~ 7750 2200
Wire Wire Line
	7750 2200 8250 2200
Wire Wire Line
	6900 1800 6900 2200
Connection ~ 6900 2200
Wire Wire Line
	6900 2200 7400 2200
Wire Wire Line
	6050 1800 6050 2200
Connection ~ 6050 2200
Wire Wire Line
	6050 2200 6550 2200
Wire Wire Line
	5200 1800 5200 2200
Connection ~ 5200 2200
Wire Wire Line
	5200 2200 5700 2200
Wire Wire Line
	4350 1800 4350 2200
Connection ~ 4350 2200
Wire Wire Line
	4350 2200 4850 2200
Connection ~ 4350 2500
Wire Wire Line
	4350 2500 4850 2500
Connection ~ 5200 2500
Wire Wire Line
	5200 2500 5700 2500
Connection ~ 6050 2500
Wire Wire Line
	6050 2500 6550 2500
Connection ~ 6900 2500
Wire Wire Line
	6900 2500 7400 2500
Connection ~ 7750 2500
Wire Wire Line
	7750 2500 8250 2500
Connection ~ 8600 2500
Wire Wire Line
	8600 2500 9100 2500
Wire Wire Line
	9100 2500 9450 2500
Connection ~ 9100 2500
Wire Wire Line
	9450 2800 9450 3200
Connection ~ 9450 3200
Wire Wire Line
	9450 3200 10000 3200
Wire Wire Line
	8600 2800 8600 3200
Connection ~ 8600 3200
Wire Wire Line
	8600 3200 9100 3200
Wire Wire Line
	7750 2800 7750 3200
Connection ~ 7750 3200
Wire Wire Line
	7750 3200 8250 3200
Wire Wire Line
	6900 2800 6900 3200
Connection ~ 6900 3200
Wire Wire Line
	6900 3200 7400 3200
Wire Wire Line
	6050 2800 6050 3200
Connection ~ 6050 3200
Wire Wire Line
	6050 3200 6550 3200
Wire Wire Line
	5200 2800 5200 3200
Connection ~ 5200 3200
Wire Wire Line
	5200 3200 5700 3200
Wire Wire Line
	4350 2800 4350 3200
Connection ~ 4350 3200
Wire Wire Line
	4350 3200 4850 3200
Wire Wire Line
	9400 1850 9800 1850
Wire Wire Line
	9800 2850 9400 2850
Connection ~ 4350 3500
Wire Wire Line
	4350 3500 4850 3500
Connection ~ 5200 3500
Wire Wire Line
	5200 3500 5700 3500
Connection ~ 6050 3500
Wire Wire Line
	6050 3500 6550 3500
Connection ~ 6900 3500
Wire Wire Line
	6900 3500 7400 3500
Connection ~ 7750 3500
Wire Wire Line
	7750 3500 8250 3500
Wire Wire Line
	8250 3500 8600 3500
Connection ~ 8250 3500
Wire Wire Line
	8600 3800 8600 4200
Connection ~ 8600 4200
Wire Wire Line
	8600 4200 10000 4200
Wire Wire Line
	7750 3800 7750 4200
Connection ~ 7750 4200
Wire Wire Line
	7750 4200 8250 4200
Wire Wire Line
	6900 3800 6900 4200
Connection ~ 6900 4200
Wire Wire Line
	6900 4200 7400 4200
Wire Wire Line
	6050 3800 6050 4200
Connection ~ 6050 4200
Wire Wire Line
	6050 4200 6550 4200
Wire Wire Line
	5200 3800 5200 4200
Connection ~ 5200 4200
Wire Wire Line
	5200 4200 5700 4200
Wire Wire Line
	4350 3800 4350 4200
Connection ~ 4350 4200
Wire Wire Line
	4350 4200 4850 4200
Connection ~ 4350 4500
Wire Wire Line
	4350 4500 4850 4500
Connection ~ 5200 4500
Wire Wire Line
	5200 4500 5700 4500
Connection ~ 6050 4500
Wire Wire Line
	6050 4500 6550 4500
Connection ~ 6900 4500
Wire Wire Line
	6900 4500 7400 4500
Connection ~ 7750 4500
Wire Wire Line
	7750 4500 8250 4500
Connection ~ 8600 4500
Wire Wire Line
	8600 4500 9100 4500
Wire Wire Line
	9450 4500 9100 4500
Connection ~ 9100 4500
Wire Wire Line
	9450 4800 9450 5200
Connection ~ 9450 5200
Wire Wire Line
	9450 5200 10000 5200
Wire Wire Line
	8600 4800 8600 5200
Connection ~ 8600 5200
Wire Wire Line
	8600 5200 9100 5200
Wire Wire Line
	7750 4800 7750 5200
Connection ~ 7750 5200
Wire Wire Line
	7750 5200 8250 5200
Wire Wire Line
	6900 4800 6900 5200
Connection ~ 6900 5200
Wire Wire Line
	6900 5200 7400 5200
Wire Wire Line
	6050 4800 6050 5200
Connection ~ 6050 5200
Wire Wire Line
	6050 5200 6550 5200
Wire Wire Line
	5200 4800 5200 5200
Connection ~ 5200 5200
Wire Wire Line
	5200 5200 5700 5200
Wire Wire Line
	4350 4800 4350 5200
Connection ~ 4350 5200
Wire Wire Line
	4350 5200 4850 5200
Connection ~ 4350 5500
Wire Wire Line
	4350 5500 4850 5500
Connection ~ 5200 5500
Wire Wire Line
	5200 5500 5700 5500
Connection ~ 6050 5500
Wire Wire Line
	6050 5500 6550 5500
Connection ~ 6900 5500
Wire Wire Line
	6900 5500 7400 5500
Connection ~ 7750 5500
Wire Wire Line
	7750 5500 8250 5500
Wire Wire Line
	8600 5500 8250 5500
Connection ~ 8250 5500
Wire Wire Line
	8600 5800 8600 6200
Connection ~ 8600 6200
Wire Wire Line
	8600 6200 10000 6200
Wire Wire Line
	7750 5800 7750 6200
Connection ~ 7750 6200
Wire Wire Line
	7750 6200 8250 6200
Wire Wire Line
	6900 5800 6900 6200
Connection ~ 6900 6200
Wire Wire Line
	6900 6200 6550 6200
Wire Wire Line
	6050 5800 6050 6200
Connection ~ 6050 6200
Wire Wire Line
	6050 6200 5700 6200
Wire Wire Line
	5200 5800 5200 6200
Connection ~ 5200 6200
Wire Wire Line
	5200 6200 5700 6200
Wire Wire Line
	4350 5800 4350 6200
Connection ~ 4350 6200
Wire Wire Line
	4350 6200 4000 6200
Wire Wire Line
	2500 3150 2500 6200
Wire Wire Line
	3150 1500 3150 2500
Wire Wire Line
	3150 2500 3150 3500
Connection ~ 3150 2500
Wire Wire Line
	3150 3500 3150 4500
Connection ~ 3150 3500
Wire Wire Line
	3150 4500 3150 5500
Connection ~ 3150 4500
Wire Wire Line
	2500 1500 3150 1500
Connection ~ 3150 1500
Wire Wire Line
	3150 5500 3150 5700
Connection ~ 3150 5500
Wire Wire Line
	3150 6000 3150 6200
Wire Wire Line
	3150 6200 4000 6200
Connection ~ 4000 6200
Wire Wire Line
	3150 6200 2500 6200
Connection ~ 3150 6200
Wire Wire Line
	2500 6200 2500 6300
Connection ~ 2500 6200
Wire Wire Line
	2500 6700 2500 6800
Wire Wire Line
	2100 6500 2200 6500
Wire Wire Line
	2500 1500 2500 2550
Wire Wire Line
	1550 1800 1550 2850
Text Label 2200 2850 2    50   ~ 0
mosi
Wire Wire Line
	10000 2200 10000 3200
Connection ~ 10000 3200
Wire Wire Line
	10000 3200 10000 4200
Connection ~ 10000 4200
Wire Wire Line
	10000 4200 10000 5200
Connection ~ 10000 5200
Wire Wire Line
	10000 5200 10000 6200
Wire Wire Line
	9800 1850 9800 2850
Wire Wire Line
	3700 2850 3600 2850
Wire Wire Line
	3600 2850 3600 3850
Wire Wire Line
	3600 3850 3700 3850
Wire Wire Line
	3700 4850 3600 4850
Wire Wire Line
	3600 4850 3600 5850
Wire Wire Line
	3600 5850 3700 5850
Wire Wire Line
	8550 3850 9800 3850
Wire Wire Line
	9800 3850 9800 4850
Wire Wire Line
	9800 4850 9400 4850
Wire Wire Line
	2900 2850 3000 2850
Wire Wire Line
	3000 2850 3000 1850
Wire Wire Line
	3000 1850 3700 1850
NoConn ~ 2150 1600
Wire Wire Line
	1450 6500 1800 6500
Wire Wire Line
	1650 1700 1450 1700
Wire Wire Line
	1450 1700 1450 6500
NoConn ~ 1650 1600
$Sheet
S 10000 1000 1000 500 
U 6019F73E
F0 "Holes" 50
F1 "holes.sch" 50
$EndSheet
$EndSCHEMATC
