EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Mechanical:MountingHole MK6
U 1 1 5AFD4177
P 2900 1950
F 0 "MK6" H 2900 2150 50  0000 C BNN
F 1 "Mounting_Hole" H 2900 2075 50  0000 C CNN
F 2 "MountingHole:MountingHole_2.2mm_M2" H 2900 1950 50  0001 C CNN
F 3 "" H 2900 1950 50  0001 C CNN
	1    2900 1950
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole MK3
U 1 1 5AFD44A2
P 2900 1600
F 0 "MK3" H 2900 1800 50  0000 C BNN
F 1 "Mounting_Hole" H 2900 1725 50  0000 C CNN
F 2 "MountingHole:MountingHole_2.2mm_M2" H 2900 1600 50  0001 C CNN
F 3 "" H 2900 1600 50  0001 C CNN
	1    2900 1600
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole MK8
U 1 1 5AFD47BD
P 2350 2300
F 0 "MK8" H 2350 2500 50  0000 C BNN
F 1 "Mounting_Hole" H 2350 2425 50  0000 C CNN
F 2 "MountingHole:MountingHole_2.2mm_M2" H 2350 2300 50  0001 C CNN
F 3 "" H 2350 2300 50  0001 C CNN
	1    2350 2300
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole MK5
U 1 1 5AFD4B28
P 2350 1950
F 0 "MK5" H 2350 2150 50  0000 C BNN
F 1 "Mounting_Hole" H 2350 2075 50  0000 C CNN
F 2 "MountingHole:MountingHole_2.2mm_M2" H 2350 1950 50  0001 C CNN
F 3 "" H 2350 1950 50  0001 C CNN
	1    2350 1950
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole MK9
U 1 1 5E1E462B
P 2900 2300
F 0 "MK9" H 2900 2500 50  0000 C BNN
F 1 "Mounting_Hole" H 2900 2425 50  0000 C CNN
F 2 "MountingHole:MountingHole_2.2mm_M2" H 2900 2300 50  0001 C CNN
F 3 "" H 2900 2300 50  0001 C CNN
	1    2900 2300
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole MK7
U 1 1 5AFD4314
P 1800 2300
F 0 "MK7" H 1800 2500 50  0000 C BNN
F 1 "Mounting_Hole" H 1800 2425 50  0000 C CNN
F 2 "MountingHole:MountingHole_2.2mm_M2" H 1800 2300 50  0001 C CNN
F 3 "" H 1800 2300 50  0001 C CNN
	1    1800 2300
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole MK4
U 1 1 5AFD462B
P 1800 1950
F 0 "MK4" H 1800 2150 50  0000 C BNN
F 1 "Mounting_Hole" H 1800 2075 50  0000 C CNN
F 2 "MountingHole:MountingHole_2.2mm_M2" H 1800 1950 50  0001 C CNN
F 3 "" H 1800 1950 50  0001 C CNN
	1    1800 1950
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole MK2
U 1 1 5AFD3B40
P 2350 1600
F 0 "MK2" H 2350 1800 50  0000 C BNN
F 1 "Mounting_Hole" H 2350 1725 50  0000 C CNN
F 2 "MountingHole:MountingHole_2.2mm_M2" H 2350 1600 50  0001 C CNN
F 3 "" H 2350 1600 50  0001 C CNN
	1    2350 1600
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole MK1
U 1 1 5AFD4950
P 1800 1600
F 0 "MK1" H 1800 1800 50  0000 C BNN
F 1 "Mounting_Hole" H 1800 1725 50  0000 C CNN
F 2 "MountingHole:MountingHole_2.2mm_M2" H 1800 1600 50  0001 C CNN
F 3 "" H 1800 1600 50  0001 C CNN
	1    1800 1600
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole MK10
U 1 1 5ECB6387
P 2100 2700
F 0 "MK10" H 2100 2900 50  0000 C BNN
F 1 "Mounting_Hole" H 2100 2825 50  0000 C CNN
F 2 "MountingHole:MountingHole_2.2mm_M2" H 2100 2700 50  0001 C CNN
F 3 "" H 2100 2700 50  0001 C CNN
	1    2100 2700
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole MK11
U 1 1 5ECB6742
P 2650 2700
F 0 "MK11" H 2650 2900 50  0000 C BNN
F 1 "Mounting_Hole" H 2650 2825 50  0000 C CNN
F 2 "MountingHole:MountingHole_2.2mm_M2" H 2650 2700 50  0001 C CNN
F 3 "" H 2650 2700 50  0001 C CNN
	1    2650 2700
	1    0    0    -1  
$EndComp
$EndSCHEMATC
