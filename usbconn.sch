EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 5
Title "USB connector"
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 8500 4000 2    50   BiDi ~ 0
mcuD+
Text HLabel 8500 3000 2    50   BiDi ~ 0
mcuD-
$Comp
L Connector:Conn_01x06_Female J6
U 1 1 5E1B2815
P 3000 3350
F 0 "J6" H 3028 3326 50  0000 L CNN
F 1 "HUB" H 3028 3235 50  0000 L CNN
F 2 "Connector_PinHeader_1.27mm:PinHeader_1x06_P1.27mm_Vertical" H 3000 3350 50  0001 C CNN
F 3 "~" H 3000 3350 50  0001 C CNN
	1    3000 3350
	-1   0    0    -1  
$EndComp
Text Notes 2350 2300 0    50   ~ 0
Link to one USB downlink port on USB hub.
$Comp
L power:+5V #PWR03
U 1 1 5E1AA9DA
P 3350 3050
F 0 "#PWR03" H 3350 2900 50  0001 C BNN
F 1 "+5V" H 3350 3190 50  0000 C CNN
F 2 "" H 3350 3050 50  0001 C CNN
F 3 "" H 3350 3050 50  0001 C CNN
	1    3350 3050
	-1   0    0    -1  
$EndComp
Wire Wire Line
	3200 3150 3350 3150
Wire Notes Line
	2350 2300 4300 2300
Wire Wire Line
	3350 3050 3350 3150
Wire Notes Line
	2350 2300 2350 4850
Wire Notes Line
	4300 4850 4300 2300
Wire Notes Line
	2350 4850 4300 4850
$Comp
L power:GND #PWR04
U 1 1 5E1A9B2F
P 3300 3750
F 0 "#PWR04" H 3300 3500 50  0001 C CNN
F 1 "GND" H 3305 3577 50  0000 C CNN
F 2 "" H 3300 3750 50  0001 C CNN
F 3 "" H 3300 3750 50  0001 C CNN
	1    3300 3750
	-1   0    0    -1  
$EndComp
$Comp
L Device:C C4
U 1 1 5AEA368F
P 6400 3100
F 0 "C4" H 6450 3200 50  0000 C BNN
F 1 "100n" H 6425 3000 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 6438 2950 50  0001 C CNN
F 3 "" H 6400 3100 50  0001 C CNN
	1    6400 3100
	1    0    0    -1  
$EndComp
Text Label 7400 3000 0    50   ~ 0
mcuD-
Text Label 7400 4000 0    50   ~ 0
mcuD+
Wire Wire Line
	6400 2850 7950 2850
Wire Wire Line
	7950 2850 7950 3500
Wire Wire Line
	6400 2850 6400 2700
NoConn ~ 5950 3950
NoConn ~ 5950 3850
Wire Wire Line
	6100 3550 6100 3650
Wire Wire Line
	6100 3650 5950 3650
Wire Notes Line
	8300 2300 4600 2300
Wire Wire Line
	5950 3550 6100 3550
Wire Wire Line
	5950 3450 6100 3450
Wire Wire Line
	6100 3450 6100 3350
Wire Wire Line
	6100 3350 5950 3350
Text Notes 4600 2300 0    50   ~ 0
USB-C optionnal connector, only if USB HUB option is NOT used.
$Comp
L power:+5V #PWR06
U 1 1 5AE837A9
P 6400 2700
F 0 "#PWR06" H 6400 2550 50  0001 C BNN
F 1 "+5V" H 6400 2840 50  0000 C CNN
F 2 "" H 6400 2700 50  0001 C CNN
F 3 "" H 6400 2700 50  0001 C CNN
	1    6400 2700
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR02
U 1 1 5AD20A2F
P 6400 5650
F 0 "#PWR02" H 6400 5400 50  0001 C BNN
F 1 "GND" H 6400 5500 50  0000 C CNN
F 2 "" H 6400 5650 50  0001 C CNN
F 3 "" H 6400 5650 50  0001 C CNN
	1    6400 5650
	1    0    0    -1  
$EndComp
NoConn ~ 5950 3050
NoConn ~ 5950 3150
Connection ~ 6400 2850
Wire Wire Line
	5950 2850 6400 2850
Wire Wire Line
	7300 4000 8500 4000
Wire Wire Line
	3600 3450 3200 3450
Text Label 7000 3000 2    50   ~ 0
D-
Text Label 7000 4000 2    50   ~ 0
D+
Wire Wire Line
	7300 3000 8500 3000
Wire Wire Line
	3600 3350 3200 3350
Text Label 3600 3350 2    50   ~ 0
D-
Text Label 3600 3450 2    50   ~ 0
D+
Wire Wire Line
	6400 3250 6400 3500
Connection ~ 6400 3500
Wire Wire Line
	6400 2850 6400 2950
Wire Wire Line
	6100 3350 6700 3350
Wire Wire Line
	6700 3350 6700 3000
Wire Wire Line
	6700 3000 7100 3000
Connection ~ 6100 3350
Wire Wire Line
	6100 3650 6700 3650
Wire Wire Line
	6700 4000 7100 4000
Connection ~ 6100 3650
Wire Wire Line
	6700 3650 6700 4000
Wire Wire Line
	3350 3150 3350 3650
Wire Wire Line
	3350 3650 3200 3650
Connection ~ 3350 3150
Wire Wire Line
	3200 3250 3300 3250
Wire Wire Line
	3300 3250 3300 3550
Wire Wire Line
	3200 3550 3300 3550
Connection ~ 3300 3550
Wire Wire Line
	3300 3550 3300 3750
$Comp
L Power_Protection:USBLC6-2SC6 U2
U 1 1 5FB665C1
P 7200 3500
F 0 "U2" V 7154 3944 50  0000 L CNN
F 1 "USBLC6-2SC6" V 7245 3944 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23-6" H 7200 3000 50  0001 C CNN
F 3 "https://www.st.com/resource/en/datasheet/usblc6-2.pdf" H 7400 3850 50  0001 C CNN
	1    7200 3500
	0    1    1    0   
$EndComp
Wire Wire Line
	6400 3500 6800 3500
Wire Wire Line
	7100 3000 7100 3100
Wire Wire Line
	7300 3000 7300 3100
Wire Wire Line
	7100 3900 7100 4000
Wire Wire Line
	7300 3900 7300 4000
Wire Wire Line
	7600 3500 7950 3500
NoConn ~ 5950 5050
NoConn ~ 5950 5150
NoConn ~ 5950 4150
NoConn ~ 5950 4250
NoConn ~ 5950 4450
NoConn ~ 5950 4550
NoConn ~ 5950 4750
NoConn ~ 5950 4850
Wire Wire Line
	6400 3500 6400 5550
$Comp
L Connector:USB_C_Receptacle J1
U 1 1 5FBC473A
P 5350 3850
F 0 "J1" H 5457 5117 50  0000 C CNN
F 1 "USB_C_Receptacle" H 5457 5026 50  0000 C CNN
F 2 "ReSplit66:USB_C_Receptacle_Molex_105450-0001_BothSides" H 5500 3850 50  0001 C CNN
F 3 "https://www.usb.org/sites/default/files/documents/usb_type-c.zip" H 5500 3850 50  0001 C CNN
	1    5350 3850
	1    0    0    -1  
$EndComp
Wire Wire Line
	5350 5450 5350 5550
Wire Wire Line
	5350 5550 6400 5550
Connection ~ 6400 5550
Wire Wire Line
	6400 5550 6400 5650
Wire Wire Line
	5050 5450 5050 5550
Wire Wire Line
	5050 5550 5350 5550
Connection ~ 5350 5550
Wire Notes Line
	4600 6000 8300 6000
Wire Notes Line
	8300 2300 8300 6000
Wire Notes Line
	4600 2300 4600 6000
$EndSCHEMATC
