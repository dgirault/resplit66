EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 5 5
Title "Processor"
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:C C1
U 1 1 5E160260
P 1300 2400
F 0 "C1" V 1250 2450 50  0000 L BNN
F 1 "10n" V 1350 2450 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 1338 2250 50  0001 C CNN
F 3 "" H 1300 2400 50  0001 C CNN
	1    1300 2400
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR0122
U 1 1 5E15A655
P 1700 2700
F 0 "#PWR0122" H 1700 2450 50  0001 C BNN
F 1 "GND" H 1700 2550 50  0000 C CNN
F 2 "" H 1700 2700 50  0001 C CNN
F 3 "" H 1700 2700 50  0001 C CNN
	1    1700 2700
	1    0    0    -1  
$EndComp
Wire Wire Line
	3600 2600 3100 2600
Text HLabel 3100 2600 0    50   Output ~ 0
led0
Wire Wire Line
	3600 2700 3100 2700
Text HLabel 3100 2700 0    50   Output ~ 0
led1
Text HLabel 3100 2800 0    50   Output ~ 0
led2
Wire Wire Line
	3600 2800 3100 2800
Text Label 4300 5200 1    50   ~ 0
vssa
Text Label 4300 1400 3    50   ~ 0
vdda
Text HLabel 3100 3000 0    50   Input ~ 0
row0
Text HLabel 3100 3100 0    50   Input ~ 0
row1
Text HLabel 3100 3200 0    50   Input ~ 0
row2
Wire Wire Line
	4800 3200 5300 3200
Wire Wire Line
	4800 3100 5300 3100
Wire Wire Line
	4800 3000 5300 3000
Wire Wire Line
	4800 3400 5300 3400
Wire Wire Line
	4800 3300 5300 3300
Wire Wire Line
	4300 1800 4300 1400
Wire Wire Line
	4300 4700 4300 5200
Wire Wire Line
	3600 4100 3100 4100
Wire Wire Line
	3600 4000 3100 4000
Wire Wire Line
	7850 1450 7850 1550
Wire Wire Line
	7450 1450 7850 1450
$Comp
L power:+3.3V #PWR0124
U 1 1 5E09656B
P 7450 1350
F 0 "#PWR0124" H 7450 1200 50  0001 C BNN
F 1 "+3.3V" H 7465 1523 50  0000 C CNN
F 2 "" H 7450 1350 50  0001 C CNN
F 3 "" H 7450 1350 50  0001 C CNN
	1    7450 1350
	1    0    0    -1  
$EndComp
Wire Wire Line
	7450 1350 7450 1450
Wire Wire Line
	6850 1550 6850 1450
Wire Wire Line
	6850 1450 7150 1450
Connection ~ 7150 1450
Wire Wire Line
	7150 1550 7150 1450
Wire Wire Line
	6850 1950 6850 1850
$Comp
L Device:C C8
U 1 1 5E167F0C
P 6850 1700
F 0 "C8" H 6750 1800 50  0000 C BNN
F 1 "100n" H 6875 1600 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 6888 1550 50  0001 C CNN
F 3 "Near VSS_1 & VDD_1" H 6850 1700 50  0001 C CNN
	1    6850 1700
	1    0    0    -1  
$EndComp
$Comp
L Device:C C9
U 1 1 5E167F19
P 7150 1700
F 0 "C9" H 7050 1800 50  0000 C BNN
F 1 "100n" H 7175 1600 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 7188 1550 50  0001 C CNN
F 3 "Near VSS_2 & VDD_2" H 7150 1700 50  0001 C CNN
	1    7150 1700
	1    0    0    -1  
$EndComp
Wire Wire Line
	7150 1950 7450 1950
Wire Wire Line
	6850 1950 7150 1950
Wire Wire Line
	3600 3000 3100 3000
Wire Wire Line
	3600 3100 3100 3100
Wire Wire Line
	3600 3200 3100 3200
Wire Wire Line
	5300 3500 4800 3500
Wire Wire Line
	5300 3600 4800 3600
Text HLabel 5300 3000 2    50   Output ~ 0
col0
Text HLabel 5300 3100 2    50   Output ~ 0
col1
Text HLabel 5300 3200 2    50   Output ~ 0
col2
Wire Wire Line
	7450 1450 7450 1550
Connection ~ 7450 1450
Wire Wire Line
	7450 1850 7450 1950
$Comp
L Device:C C10
U 1 1 5E167F29
P 7450 1700
F 0 "C10" H 7350 1800 50  0000 C BNN
F 1 "100n" H 7475 1600 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 7488 1550 50  0001 C CNN
F 3 "" H 7450 1700 50  0001 C CNN
	1    7450 1700
	1    0    0    -1  
$EndComp
Wire Wire Line
	7150 1450 7450 1450
Wire Wire Line
	7150 1950 7150 1850
$Comp
L power:GND #PWR0123
U 1 1 5E167F21
P 7450 2050
F 0 "#PWR0123" H 7450 1800 50  0001 C BNN
F 1 "GND" H 7450 1900 50  0000 C CNN
F 2 "" H 7450 2050 50  0001 C CNN
F 3 "" H 7450 2050 50  0001 C CNN
	1    7450 2050
	1    0    0    -1  
$EndComp
Wire Wire Line
	7450 1950 7450 2050
Connection ~ 7450 1950
Connection ~ 7150 1950
Wire Wire Line
	7850 1850 7850 1950
$Comp
L Device:C C11
U 1 1 5E0CA431
P 9750 1700
F 0 "C11" H 9650 1800 50  0000 C BNN
F 1 "10n" H 9775 1600 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 9788 1550 50  0001 C CNN
F 3 "Near VSSA & VDDA" H 9750 1700 50  0001 C CNN
	1    9750 1700
	1    0    0    -1  
$EndComp
Connection ~ 7850 1950
Wire Wire Line
	7450 1950 7850 1950
$Comp
L Device:C C12
U 1 1 5E0FA6CD
P 7850 1700
F 0 "C12" H 7750 1800 50  0000 C BNN
F 1 "4.7u" H 7875 1600 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 7888 1550 50  0001 C CNN
F 3 "Near 3.3V LDO Output" H 7850 1700 50  0001 C CNN
	1    7850 1700
	1    0    0    -1  
$EndComp
$Comp
L Device:C C13
U 1 1 5E0FD721
P 9350 1700
F 0 "C13" H 9250 1800 50  0000 C BNN
F 1 "1u" H 9375 1600 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 9388 1550 50  0001 C CNN
F 3 "Near VSSA & VDDA" H 9350 1700 50  0001 C CNN
	1    9350 1700
	1    0    0    -1  
$EndComp
Text Label 8250 1450 2    50   ~ 0
vdd
Text Label 8250 1950 2    50   ~ 0
vss
Text Label 3100 3300 0    50   ~ 0
traceswo
Text Label 3100 3700 0    50   ~ 0
i2c1_sda
Text Label 3100 2400 0    50   ~ 0
boot0
Wire Wire Line
	3600 3700 3100 3700
Wire Wire Line
	3600 3600 3100 3600
Wire Wire Line
	4200 4700 4200 4800
Text Label 4200 5200 1    50   ~ 0
vss
Text Label 4200 1400 3    50   ~ 0
vdd
Text HLabel 3100 3700 0    50   BiDi ~ 0
sda
Text HLabel 3100 3600 0    50   BiDi ~ 0
scl
Text HLabel 5300 3500 2    50   Output ~ 0
col5
Text HLabel 5300 3600 2    50   Output ~ 0
col6
$Comp
L Switch:SW_Push Boot0
U 1 1 5E1956A1
P 2950 1700
F 0 "Boot0" H 2950 1985 50  0000 C CNN
F 1 "SW_Push" H 2950 1894 50  0000 C CNN
F 2 "ReSplit66:Tact_Switch_Side_Boss_B3U-3000P-B_BothSides_HandSolder" H 2950 1900 50  0001 C CNN
F 3 "" H 2950 1900 50  0001 C CNN
	1    2950 1700
	-1   0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR0128
U 1 1 5E0D0C15
P 1700 1700
F 0 "#PWR0128" H 1700 1550 50  0001 C BNN
F 1 "+3.3V" H 1715 1873 50  0000 C CNN
F 2 "" H 1700 1700 50  0001 C CNN
F 3 "" H 1700 1700 50  0001 C CNN
	1    1700 1700
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0125
U 1 1 5E1956A8
P 2650 2800
F 0 "#PWR0125" H 2650 2550 50  0001 C BNN
F 1 "GND" H 2650 2650 50  0000 C CNN
F 2 "" H 2650 2800 50  0001 C CNN
F 3 "" H 2650 2800 50  0001 C CNN
	1    2650 2800
	1    0    0    -1  
$EndComp
$Comp
L Device:R R3
U 1 1 5E0BAA02
P 2650 2600
F 0 "R3" H 2720 2554 50  0000 L CNN
F 1 "10k" H 2720 2645 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 2580 2600 50  0001 C CNN
F 3 "~" H 2650 2600 50  0001 C CNN
	1    2650 2600
	-1   0    0    1   
$EndComp
Wire Wire Line
	3600 3300 3100 3300
Text HLabel 5300 3300 2    50   Output ~ 0
col3
Text HLabel 5300 3400 2    50   Output ~ 0
col4
Text Label 3100 3600 0    50   ~ 0
i2c1_scl
Wire Wire Line
	3600 3400 3100 3400
Wire Wire Line
	3600 3500 3100 3500
Wire Wire Line
	4800 4400 5300 4400
Text Label 5300 4400 2    50   ~ 0
swclk
Text Label 5300 4300 2    50   ~ 0
swdio
Wire Wire Line
	4800 4300 5300 4300
Text HLabel 5300 4200 2    50   BiDi ~ 0
usbD+
Text HLabel 5300 4100 2    50   BiDi ~ 0
usbD-
Wire Wire Line
	4800 4000 5300 4000
Wire Wire Line
	4800 4100 5300 4100
Wire Wire Line
	4800 3900 5300 3900
Text Label 5300 4000 2    50   ~ 0
uart1rx
Text Label 5300 3900 2    50   ~ 0
uart1tx
Wire Wire Line
	4800 3800 5300 3800
Wire Wire Line
	3600 4500 3100 4500
Wire Wire Line
	3600 4400 3100 4400
Wire Wire Line
	3600 4300 3100 4300
Wire Wire Line
	3600 4200 3100 4200
$Comp
L power:+3.3V #PWR0107
U 1 1 5E28CE7C
P 9750 1350
F 0 "#PWR0107" H 9750 1200 50  0001 C BNN
F 1 "+3.3V" H 9765 1523 50  0000 C CNN
F 2 "" H 9750 1350 50  0001 C CNN
F 3 "" H 9750 1350 50  0001 C CNN
	1    9750 1350
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0108
U 1 1 5E290D6B
P 9750 2050
F 0 "#PWR0108" H 9750 1800 50  0001 C BNN
F 1 "GND" H 9750 1900 50  0000 C CNN
F 2 "" H 9750 2050 50  0001 C CNN
F 3 "" H 9750 2050 50  0001 C CNN
	1    9750 2050
	1    0    0    -1  
$EndComp
Wire Wire Line
	9750 1950 9750 2050
Wire Wire Line
	7850 1950 8250 1950
Connection ~ 7850 1450
Wire Wire Line
	7850 1450 8250 1450
Wire Wire Line
	9750 1350 9750 1450
Wire Wire Line
	9750 1450 9350 1450
Wire Wire Line
	9350 1450 9350 1550
Connection ~ 9750 1450
Wire Wire Line
	9750 1450 9750 1550
Wire Wire Line
	9350 1850 9350 1950
Wire Wire Line
	9350 1950 9750 1950
Wire Wire Line
	9750 1850 9750 1950
Connection ~ 9750 1950
Wire Wire Line
	9350 1450 9000 1450
Connection ~ 9350 1450
Wire Wire Line
	9350 1950 9000 1950
Connection ~ 9350 1950
Text Label 9000 1450 0    50   ~ 0
vdda
Text Label 9000 1950 0    50   ~ 0
vssa
Text HLabel 3100 3500 0    50   Input ~ 0
row4
Text HLabel 3100 3400 0    50   Input ~ 0
row3
Wire Wire Line
	3600 3900 3100 3900
Wire Wire Line
	3600 3800 3100 3800
Wire Wire Line
	4800 4500 5300 4500
Text Label 8050 3400 2    50   ~ 0
nrst
$Comp
L power:+3.3V #PWR0126
U 1 1 5E0C1077
P 7150 2900
F 0 "#PWR0126" H 7150 2750 50  0001 C BNN
F 1 "+3.3V" H 7165 3073 50  0000 C CNN
F 2 "" H 7150 2900 50  0001 C CNN
F 3 "" H 7150 2900 50  0001 C CNN
	1    7150 2900
	1    0    0    -1  
$EndComp
Wire Wire Line
	8050 3400 7750 3400
Wire Wire Line
	8050 3100 7750 3100
Wire Wire Line
	7750 3000 8050 3000
Wire Wire Line
	7750 3200 8050 3200
Text Label 8050 3000 2    50   ~ 0
swdio
Text Label 8050 3200 2    50   ~ 0
traceswo
Text Label 8050 3100 2    50   ~ 0
swclk
Wire Wire Line
	8900 3200 9350 3200
Text Label 8900 3200 0    50   ~ 0
uart1tx
Text Label 8900 3100 0    50   ~ 0
uart1rx
$Comp
L power:GND #PWR0127
U 1 1 5E0C4218
P 7150 3500
F 0 "#PWR0127" H 7150 3250 50  0001 C BNN
F 1 "GND" H 7150 3350 50  0000 C CNN
F 2 "" H 7150 3500 50  0001 C CNN
F 3 "" H 7150 3500 50  0001 C CNN
	1    7150 3500
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x05_Odd_Even J3
U 1 1 5EABF097
P 7450 3200
F 0 "J3" H 7500 3617 50  0000 C CNN
F 1 "JTAG" H 7500 3526 50  0000 C CNN
F 2 "Connector_PinHeader_1.27mm:PinHeader_2x05_P1.27mm_Horizontal" H 7450 3200 50  0001 C CNN
F 3 "~" H 7450 3200 50  0001 C CNN
	1    7450 3200
	1    0    0    -1  
$EndComp
Wire Wire Line
	7150 3000 7250 3000
NoConn ~ 7750 3300
NoConn ~ 7250 3300
Wire Wire Line
	7250 3100 7150 3100
Wire Wire Line
	7150 3100 7150 3200
Wire Wire Line
	7250 3200 7150 3200
Connection ~ 7150 3200
Wire Wire Line
	7150 3200 7150 3400
Wire Wire Line
	7250 3400 7150 3400
Connection ~ 7150 3400
Wire Wire Line
	7150 3400 7150 3500
Wire Wire Line
	9350 3100 8900 3100
$Comp
L Device:R R6
U 1 1 5EB41323
P 3350 1850
F 0 "R6" H 3420 1896 50  0000 L CNN
F 1 "10k" H 3420 1805 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 3280 1850 50  0001 C CNN
F 3 "~" H 3350 1850 50  0001 C CNN
	1    3350 1850
	1    0    0    -1  
$EndComp
Wire Wire Line
	3600 2000 3350 2000
Text Label 3100 2000 0    50   ~ 0
nrst
$Comp
L Oscillator:ASE-xxxMHz X1
U 1 1 5EB8A41A
P 1700 2200
F 0 "X1" H 1750 2450 50  0000 L CNN
F 1 "ASE-8MHz" H 1750 1950 50  0000 L CNN
F 2 "ReSplit66:Oscillator_SMD_Abracon_ASE-4Pin_3.2x2.5mm" H 2400 1850 50  0001 C CNN
F 3 "http://www.abracon.com/Oscillators/ASV.pdf" H 1600 2200 50  0001 C CNN
	1    1700 2200
	1    0    0    -1  
$EndComp
Wire Wire Line
	1400 2200 1300 2200
Wire Wire Line
	1300 2200 1300 1800
Wire Wire Line
	1300 1800 1700 1800
Wire Wire Line
	1700 1800 1700 1900
$Comp
L Connector_Generic:Conn_01x04 J4
U 1 1 5EC130DE
P 9550 3100
F 0 "J4" H 9630 3092 50  0000 L CNN
F 1 "UART" H 9630 3001 50  0000 L CNN
F 2 "Connector_Molex:Molex_PicoBlade_53048-0410_1x04_P1.25mm_Horizontal" H 9550 3100 50  0001 C CNN
F 3 "~" H 9550 3100 50  0001 C CNN
	1    9550 3100
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x04_Odd_Even J7
U 1 1 5ECE9936
P 9450 4600
F 0 "J7" H 9500 4917 50  0000 C CNN
F 1 "Ext" H 9500 4826 50  0000 C CNN
F 2 "Connector_PinHeader_1.00mm:PinHeader_2x04_P1.00mm_Vertical" H 9450 4600 50  0001 C CNN
F 3 "~" H 9450 4600 50  0001 C CNN
	1    9450 4600
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR05
U 1 1 5ECEAE82
P 9850 4400
F 0 "#PWR05" H 9850 4250 50  0001 C BNN
F 1 "+3.3V" H 9865 4573 50  0000 C CNN
F 2 "" H 9850 4400 50  0001 C CNN
F 3 "" H 9850 4400 50  0001 C CNN
	1    9850 4400
	1    0    0    -1  
$EndComp
Wire Wire Line
	9150 4400 9150 4500
Wire Wire Line
	9150 4500 9250 4500
$Comp
L power:GND #PWR07
U 1 1 5ECEFAC4
P 9150 4900
F 0 "#PWR07" H 9150 4650 50  0001 C BNN
F 1 "GND" H 9150 4750 50  0000 C CNN
F 2 "" H 9150 4900 50  0001 C CNN
F 3 "" H 9150 4900 50  0001 C CNN
	1    9150 4900
	1    0    0    -1  
$EndComp
Wire Wire Line
	10000 4600 9750 4600
Text Label 3100 2300 0    50   ~ 0
ph1
Wire Wire Line
	3100 2300 3600 2300
Wire Wire Line
	5300 3700 4800 3700
Text Label 5300 3700 2    50   ~ 0
pa7
Text Label 3100 3900 0    50   ~ 0
pb9
Text Label 3100 3800 0    50   ~ 0
pb8
Text Label 5300 4500 2    50   ~ 0
pa15
Text Label 9000 4600 0    50   ~ 0
pa7
Wire Wire Line
	9000 4600 9250 4600
Wire Wire Line
	9000 4700 9250 4700
Wire Wire Line
	10000 4700 9750 4700
Wire Wire Line
	10000 4800 9750 4800
Text Label 10000 4600 2    50   ~ 0
pa15
Text Label 10000 4800 2    50   ~ 0
pb9
Text Label 10000 4700 2    50   ~ 0
pb8
Text Label 9000 4700 0    50   ~ 0
ph1
Text Notes 1000 6450 0    50   ~ 10
Port J2 can only be used as link between master/slave keyboards.\nSo PB6 & PB7 pins should be I2C1 only or UART1 if cross-cable is used.\n
Text Notes 1000 7050 0    50   ~ 10
See pinout.md for detailled alternate functions available.
Text Notes 1000 6600 0    50   ~ 10
Port J4 can be USART1 TX/RX, I2C1, TIMER1 channels 2 & 3.
Text Notes 1000 6700 0    50   ~ 10
Port J5 can be GPIO, SPI2 + I2C2, SAI, TSC_IO, USART3, LPUART1, SWPMI1.
Text Notes 1000 6800 0    50   ~ 10
Port J7 is CAN controller or IR OUT or more TIMERS channels.
Wire Wire Line
	4200 1800 4200 1700
Wire Wire Line
	4800 4200 5300 4200
Wire Wire Line
	4200 1700 4100 1700
Wire Wire Line
	4100 1700 4100 1800
Connection ~ 4200 1700
Wire Wire Line
	4200 1700 4200 1400
Wire Wire Line
	4100 1700 4000 1700
Wire Wire Line
	4000 1700 4000 1800
Connection ~ 4100 1700
Connection ~ 3350 2000
Wire Wire Line
	3350 2000 3100 2000
Wire Wire Line
	3350 1700 4000 1700
Connection ~ 4000 1700
Wire Wire Line
	4000 4700 4000 4800
Wire Wire Line
	4000 4800 4100 4800
Connection ~ 4200 4800
Wire Wire Line
	4200 4800 4200 5200
Wire Wire Line
	4100 4700 4100 4800
Connection ~ 4100 4800
Wire Wire Line
	4100 4800 4200 4800
Wire Wire Line
	1700 1700 1700 1800
Connection ~ 1700 1800
Wire Wire Line
	1700 2500 1700 2600
Wire Wire Line
	1700 2600 1300 2600
Wire Wire Line
	1700 2600 1700 2700
Connection ~ 1700 2600
Wire Wire Line
	1300 2200 1300 2250
Connection ~ 1300 2200
Wire Wire Line
	1300 2550 1300 2600
Wire Wire Line
	3150 1700 3350 1700
Connection ~ 3350 1700
Wire Wire Line
	2750 1700 2650 1700
Wire Wire Line
	2650 1700 2650 2400
Wire Wire Line
	2650 2400 3600 2400
Wire Wire Line
	2650 2400 2650 2450
Connection ~ 2650 2400
Wire Wire Line
	2650 2750 2650 2800
$Comp
L MCU_ST_STM32L4:STM32L443CCTx U1
U 1 1 5FB36CFC
P 4200 3200
F 0 "U1" H 4400 1700 50  0000 L CNN
F 1 "STM32L443CCTx" H 4700 1600 50  0000 C CNN
F 2 "Package_QFP:LQFP-48_7x7mm_P0.5mm" H 3700 1800 50  0001 R CNN
F 3 "http://www.st.com/st-web-ui/static/active/en/resource/technical/document/datasheet/DM00254865.pdf" H 4200 3200 50  0001 C CNN
	1    4200 3200
	1    0    0    -1  
$EndComp
Wire Wire Line
	3600 2200 2000 2200
Wire Wire Line
	4200 1700 4400 1700
Wire Wire Line
	4400 1700 4400 1800
Wire Wire Line
	7150 2900 7150 3000
$Comp
L power:GND #PWR0109
U 1 1 5FD910C9
P 9250 3400
F 0 "#PWR0109" H 9250 3150 50  0001 C BNN
F 1 "GND" H 9250 3250 50  0000 C CNN
F 2 "" H 9250 3400 50  0001 C CNN
F 3 "" H 9250 3400 50  0001 C CNN
	1    9250 3400
	1    0    0    -1  
$EndComp
Wire Wire Line
	9350 3300 9250 3300
Wire Wire Line
	9250 3300 9250 3400
Wire Wire Line
	9250 4800 9150 4800
Wire Wire Line
	9150 4800 9150 4900
Wire Wire Line
	9750 4500 9850 4500
Wire Wire Line
	9850 4500 9850 4400
$Comp
L power:+5V #PWR0110
U 1 1 5FDE3308
P 9150 4400
F 0 "#PWR0110" H 9150 4250 50  0001 C CNN
F 1 "+5V" H 9165 4573 50  0000 C CNN
F 2 "" H 9150 4400 50  0001 C CNN
F 3 "" H 9150 4400 50  0001 C CNN
	1    9150 4400
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0111
U 1 1 5FDE3816
P 9250 2900
F 0 "#PWR0111" H 9250 2750 50  0001 C CNN
F 1 "+5V" H 9265 3073 50  0000 C CNN
F 2 "" H 9250 2900 50  0001 C CNN
F 3 "" H 9250 2900 50  0001 C CNN
	1    9250 2900
	1    0    0    -1  
$EndComp
Wire Wire Line
	9250 2900 9250 3000
Wire Wire Line
	9250 3000 9350 3000
Wire Wire Line
	6950 4900 7250 4900
Wire Wire Line
	6950 4700 7250 4700
Wire Wire Line
	7150 4500 7250 4500
Wire Wire Line
	6950 4800 7250 4800
Wire Wire Line
	7750 4900 7850 4900
Wire Wire Line
	7250 4600 6950 4600
Wire Wire Line
	7150 4500 7150 4250
$Comp
L power:+3.3V #PWR?
U 1 1 5FB93F5F
P 7150 4250
AR Path="/5E103213/5FB93F5F" Ref="#PWR?"  Part="1" 
AR Path="/5E07FA78/5FB93F5F" Ref="#PWR0103"  Part="1" 
F 0 "#PWR0103" H 7150 4100 50  0001 C CNN
F 1 "+3.3V" H 7165 4423 50  0000 C CNN
F 2 "" H 7150 4250 50  0001 C CNN
F 3 "" H 7150 4250 50  0001 C CNN
	1    7150 4250
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 5FB93F65
P 7850 4250
AR Path="/5E103213/5FB93F65" Ref="#PWR?"  Part="1" 
AR Path="/5E07FA78/5FB93F65" Ref="#PWR0104"  Part="1" 
F 0 "#PWR0104" H 7850 4100 50  0001 C CNN
F 1 "+5V" H 7865 4423 50  0000 C CNN
F 2 "" H 7850 4250 50  0001 C CNN
F 3 "" H 7850 4250 50  0001 C CNN
	1    7850 4250
	1    0    0    -1  
$EndComp
Wire Wire Line
	7850 4250 7850 4500
$Comp
L Connector_Generic:Conn_02x05_Odd_Even J?
U 1 1 5FB93F6C
P 7450 4700
AR Path="/5E103213/5FB93F6C" Ref="J?"  Part="1" 
AR Path="/5E07FA78/5FB93F6C" Ref="J5"  Part="1" 
F 0 "J5" H 7500 5117 50  0000 C CNN
F 1 "RGB_Oled" H 7500 5026 50  0000 C CNN
F 2 "Connector_PinHeader_1.27mm:PinHeader_2x05_P1.27mm_Vertical" H 7450 4700 50  0001 C CNN
F 3 "~" H 7450 4700 50  0001 C CNN
	1    7450 4700
	1    0    0    -1  
$EndComp
Wire Wire Line
	7750 4800 8050 4800
Wire Wire Line
	7750 4700 8050 4700
$Comp
L power:GND #PWR?
U 1 1 5FB93F77
P 7850 5000
AR Path="/5E103213/5FB93F77" Ref="#PWR?"  Part="1" 
AR Path="/5E07FA78/5FB93F77" Ref="#PWR0105"  Part="1" 
F 0 "#PWR0105" H 7850 4750 50  0001 C CNN
F 1 "GND" H 7855 4827 50  0000 C CNN
F 2 "" H 7850 5000 50  0001 C CNN
F 3 "" H 7850 5000 50  0001 C CNN
	1    7850 5000
	1    0    0    -1  
$EndComp
Wire Wire Line
	7750 4600 8050 4600
Wire Wire Line
	7750 4500 7850 4500
Wire Wire Line
	7850 4900 7850 5000
Text Label 3100 4000 0    50   ~ 0
pb10
Text Label 3100 4100 0    50   ~ 0
pb11
Text Label 3100 4200 0    50   ~ 0
nss
Text Label 3100 4300 0    50   ~ 0
sck
Text Label 3100 4400 0    50   ~ 0
miso
Text Label 3100 4500 0    50   ~ 0
mosi
Text Label 5300 3800 2    50   ~ 0
pa8
Text Label 8050 4700 2    50   ~ 0
pa8
Text Label 8050 4600 2    50   ~ 0
nss
Text Label 6950 4600 0    50   ~ 0
sck
Text Label 6950 4700 0    50   ~ 0
miso
Text Label 6950 4800 0    50   ~ 0
mosi
Text Label 6950 4900 0    50   ~ 0
pb11
Text Label 8050 4800 2    50   ~ 0
pb10
$EndSCHEMATC
