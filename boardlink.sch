EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 5
Title "Board link"
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Label 6450 2950 0    50   ~ 0
cSDA
Text Label 6450 2850 0    50   ~ 0
cSCL
$Comp
L power:GND #PWR011
U 1 1 5AE8B0BB
P 8200 3450
F 0 "#PWR011" H 8200 3200 50  0001 C BNN
F 1 "GND" H 8200 3300 50  0000 C CNN
F 2 "" H 8200 3450 50  0001 C CNN
F 3 "" H 8200 3450 50  0001 C CNN
	1    8200 3450
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR01
U 1 1 5E1D6F8C
P 6700 2150
F 0 "#PWR01" H 6700 2000 50  0001 C CNN
F 1 "+5V" H 6715 2323 50  0000 C CNN
F 2 "" H 6700 2150 50  0001 C CNN
F 3 "" H 6700 2150 50  0001 C CNN
	1    6700 2150
	1    0    0    -1  
$EndComp
NoConn ~ 6950 3050
$Comp
L Device:C C7
U 1 1 5AEA86F2
P 8500 2850
F 0 "C7" H 8500 2900 50  0000 R BNN
F 1 "100n" H 8525 2750 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 8538 2700 50  0001 C CNN
F 3 "" H 8500 2850 50  0001 C CNN
	1    8500 2850
	1    0    0    -1  
$EndComp
$Comp
L Device:C C6
U 1 1 5AFF85C6
P 8200 2850
F 0 "C6" H 8200 2900 50  0000 R BNN
F 1 "1u" H 8225 2750 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 8238 2700 50  0001 C CNN
F 3 "" H 8200 2850 50  0001 C CNN
	1    8200 2850
	1    0    0    -1  
$EndComp
$Comp
L Device:C C5
U 1 1 5AD7B7C3
P 7900 2850
F 0 "C5" H 7900 2900 50  0000 R BNN
F 1 "1u" H 7925 2750 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 7938 2700 50  0001 C CNN
F 3 "" H 7900 2850 50  0001 C CNN
	1    7900 2850
	1    0    0    -1  
$EndComp
$Comp
L Device:R R2
U 1 1 5AE8C8F6
P 3200 2450
F 0 "R2" V 3300 2450 50  0000 C BNN
F 1 "4k7" V 3200 2450 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 3130 2450 50  0001 C CNN
F 3 "" H 3200 2450 50  0001 C CNN
	1    3200 2450
	1    0    0    -1  
$EndComp
$Comp
L Device:R R1
U 1 1 5AE8D451
P 3000 2450
F 0 "R1" V 3100 2450 50  0000 C BNN
F 1 "4k7" V 3000 2450 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 2930 2450 50  0001 C CNN
F 3 "" H 3000 2450 50  0001 C CNN
	1    3000 2450
	1    0    0    -1  
$EndComp
Wire Wire Line
	3000 2300 3000 2200
$Comp
L power:+3.3V #PWR0106
U 1 1 5E0CD712
P 3000 2200
F 0 "#PWR0106" H 3000 2050 50  0001 C BNN
F 1 "+3.3V" H 3015 2373 50  0000 C CNN
F 2 "" H 3000 2200 50  0001 C CNN
F 3 "" H 3000 2200 50  0001 C CNN
	1    3000 2200
	1    0    0    -1  
$EndComp
Connection ~ 3000 2300
Wire Wire Line
	3200 2300 3000 2300
Wire Wire Line
	3200 2950 4200 2950
Wire Wire Line
	3000 2750 4200 2750
Wire Wire Line
	3200 2600 3200 2950
Wire Wire Line
	3000 2600 3000 2750
Text HLabel 2650 2750 0    50   BiDi ~ 0
SCL
Connection ~ 3000 2750
Wire Wire Line
	2650 2750 3000 2750
Connection ~ 3200 2950
Wire Wire Line
	2650 2950 3200 2950
Text HLabel 2650 2950 0    50   BiDi ~ 0
SDA
Text Notes 2050 4450 0    50   ~ 0
The left and right sides are connected through the USB Micro B connectors.\nThese connectors allow slave side to receive directly USB +5V VBUS, because\nboth boards require 5V if Oled or RGB leds option are in use.\n\nThis result in both side neeeds the LDO (see the alim sheet) to have the needed 3.3V.\n\nQuestion:\nDoes the pull-up to 3.3V on SDA & SCL signal will be a problem ?
Wire Wire Line
	5200 2400 6450 2400
Wire Wire Line
	6450 2400 6450 2850
Wire Wire Line
	6450 2850 6950 2850
Wire Wire Line
	5200 3400 6450 3400
Wire Wire Line
	6450 3400 6450 2950
Wire Wire Line
	6450 2950 6950 2950
Wire Wire Line
	6700 2150 6700 2250
Wire Wire Line
	6700 2650 6950 2650
Connection ~ 6700 2250
Wire Wire Line
	6700 2250 6700 2650
Wire Wire Line
	6700 2250 7900 2250
Wire Wire Line
	7900 2250 7900 2700
Wire Wire Line
	7900 2250 8200 2250
Wire Wire Line
	8200 2250 8200 2700
Connection ~ 7900 2250
Wire Wire Line
	8200 2250 8500 2250
Wire Wire Line
	8500 2250 8500 2700
Connection ~ 8200 2250
Wire Wire Line
	8200 3350 8200 3450
Connection ~ 8200 3350
Wire Wire Line
	8200 3350 8500 3350
Wire Wire Line
	7250 3250 7250 3350
Wire Wire Line
	7250 3350 7350 3350
Wire Wire Line
	7900 3000 7900 3350
Connection ~ 7900 3350
Wire Wire Line
	7900 3350 8200 3350
Wire Wire Line
	8200 3000 8200 3350
Wire Wire Line
	8500 3000 8500 3350
Wire Wire Line
	4500 2900 4500 2250
Wire Wire Line
	4500 2250 6700 2250
Wire Wire Line
	5000 2400 4200 2400
Wire Wire Line
	4200 2400 4200 2750
Wire Wire Line
	4200 2950 4200 3400
Wire Wire Line
	4200 3400 5000 3400
Wire Wire Line
	7350 3250 7350 3350
Connection ~ 7350 3350
Wire Wire Line
	7350 3350 7900 3350
Wire Wire Line
	7250 3350 5600 3350
Wire Wire Line
	5600 2900 5600 3350
Connection ~ 7250 3350
$Comp
L Power_Protection:USBLC6-2SC6 U3
U 1 1 5FB76818
P 5100 2900
F 0 "U3" V 5054 2456 50  0000 R CNN
F 1 "USBLC6-2SC6" V 5145 2456 50  0000 R CNN
F 2 "Package_TO_SOT_SMD:SOT-23-6" H 5100 2400 50  0001 C CNN
F 3 "https://www.st.com/resource/en/datasheet/usblc6-2.pdf" H 5300 3250 50  0001 C CNN
	1    5100 2900
	0    -1   1    0   
$EndComp
Wire Wire Line
	4500 2900 4700 2900
Wire Wire Line
	5000 3300 5000 3400
Wire Wire Line
	5200 3300 5200 3400
Wire Wire Line
	5000 2400 5000 2500
Wire Wire Line
	5200 2400 5200 2500
Wire Wire Line
	5600 2900 5500 2900
$Comp
L Connector:USB_B_Micro J2
U 1 1 5FB80EB8
P 7250 2850
F 0 "J2" H 7020 2839 50  0000 R CNN
F 1 "Link" H 7020 2748 50  0000 R CNN
F 2 "ReSplit66:USB_Micro-B_Wuerth_629105136821_BothSide" H 7400 2800 50  0001 C CNN
F 3 "~" H 7400 2800 50  0001 C CNN
	1    7250 2850
	-1   0    0    -1  
$EndComp
$EndSCHEMATC
