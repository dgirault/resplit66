EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Mechanical:MountingHole MK6
U 1 1 5AFD4177
P 9950 4950
F 0 "MK6" H 9950 5150 50  0000 C BNN
F 1 "Mounting_Hole" H 9950 5075 50  0000 C CNN
F 2 "MountingHole:MountingHole_2.2mm_M2" H 9950 4950 50  0001 C CNN
F 3 "" H 9950 4950 50  0001 C CNN
	1    9950 4950
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole MK3
U 1 1 5AFD44A2
P 9950 4600
F 0 "MK3" H 9950 4800 50  0000 C BNN
F 1 "Mounting_Hole" H 9950 4725 50  0000 C CNN
F 2 "MountingHole:MountingHole_2.2mm_M2" H 9950 4600 50  0001 C CNN
F 3 "" H 9950 4600 50  0001 C CNN
	1    9950 4600
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole MK8
U 1 1 5AFD47BD
P 9400 5300
F 0 "MK8" H 9400 5500 50  0000 C BNN
F 1 "Mounting_Hole" H 9400 5425 50  0000 C CNN
F 2 "MountingHole:MountingHole_2.2mm_M2" H 9400 5300 50  0001 C CNN
F 3 "" H 9400 5300 50  0001 C CNN
	1    9400 5300
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole MK5
U 1 1 5AFD4B28
P 9400 4950
F 0 "MK5" H 9400 5150 50  0000 C BNN
F 1 "Mounting_Hole" H 9400 5075 50  0000 C CNN
F 2 "MountingHole:MountingHole_2.2mm_M2" H 9400 4950 50  0001 C CNN
F 3 "" H 9400 4950 50  0001 C CNN
	1    9400 4950
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole MK9
U 1 1 5E1E462B
P 9950 5300
F 0 "MK9" H 9950 5500 50  0000 C BNN
F 1 "Mounting_Hole" H 9950 5425 50  0000 C CNN
F 2 "MountingHole:MountingHole_2.2mm_M2" H 9950 5300 50  0001 C CNN
F 3 "" H 9950 5300 50  0001 C CNN
	1    9950 5300
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole MK7
U 1 1 5AFD4314
P 8850 5300
F 0 "MK7" H 8850 5500 50  0000 C BNN
F 1 "Mounting_Hole" H 8850 5425 50  0000 C CNN
F 2 "MountingHole:MountingHole_2.2mm_M2" H 8850 5300 50  0001 C CNN
F 3 "" H 8850 5300 50  0001 C CNN
	1    8850 5300
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole MK4
U 1 1 5AFD462B
P 8850 4950
F 0 "MK4" H 8850 5150 50  0000 C BNN
F 1 "Mounting_Hole" H 8850 5075 50  0000 C CNN
F 2 "MountingHole:MountingHole_2.2mm_M2" H 8850 4950 50  0001 C CNN
F 3 "" H 8850 4950 50  0001 C CNN
	1    8850 4950
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole MK2
U 1 1 5AFD3B40
P 9400 4600
F 0 "MK2" H 9400 4800 50  0000 C BNN
F 1 "Mounting_Hole" H 9400 4725 50  0000 C CNN
F 2 "MountingHole:MountingHole_2.2mm_M2" H 9400 4600 50  0001 C CNN
F 3 "" H 9400 4600 50  0001 C CNN
	1    9400 4600
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole MK1
U 1 1 5AFD4950
P 8850 4600
F 0 "MK1" H 8850 4800 50  0000 C BNN
F 1 "Mounting_Hole" H 8850 4725 50  0000 C CNN
F 2 "MountingHole:MountingHole_2.2mm_M2" H 8850 4600 50  0001 C CNN
F 3 "" H 8850 4600 50  0001 C CNN
	1    8850 4600
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole MK10
U 1 1 5FB1798F
P 9150 5700
F 0 "MK10" H 9150 5900 50  0000 C BNN
F 1 "Mounting_Hole" H 9150 5825 50  0000 C CNN
F 2 "MountingHole:MountingHole_2.2mm_M2" H 9150 5700 50  0001 C CNN
F 3 "" H 9150 5700 50  0001 C CNN
	1    9150 5700
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole MK11
U 1 1 5FB17D7A
P 9750 5700
F 0 "MK11" H 9750 5900 50  0000 C BNN
F 1 "Mounting_Hole" H 9750 5825 50  0000 C CNN
F 2 "MountingHole:MountingHole_2.2mm_M2" H 9750 5700 50  0001 C CNN
F 3 "" H 9750 5700 50  0001 C CNN
	1    9750 5700
	1    0    0    -1  
$EndComp
$EndSCHEMATC
