# GPIO assignment of STM32L443CCTx

## Pins assignment sorted by MCU pin's names

|Pin   |Port   |Default    |Alternate USABLE functions (1)                         |
|:-----|:------|:----------|:------------------------------------------------------|
|`PA0` |NA     |*Col0*     |                                                       |
|`PA1` |NA     |*Col1*     |                                                       |
|`PA2` |NA     |*Col2*     |                                                       |
|`PA3` |NA     |*Col3*     |                                                       |
|`PA4` |NA     |*Col4*     |                                                       |
|`PA5` |NA     |*Col5*     |                                                       |
|`PA6` |NA     |*Col6*     |                                                       |
|`PA7` |J7     |GPIO       |`TIM1_CH1N, I2C3_SCL, ADC1_IN12`                       |
|`PA8` |J5     |GPIO       |`MCO, TIM1_CH1, SWPMI1_IO, LPTIM2_OUT`                 |
|`PA9` |J4     |`USART1_TX`|`I2C1_SCL, SAI1_FS_A, TIM1_CH2`                        |
|`PA10`|J4     |`USART1_RX`|`I2C1_SDA, SAI1_SD_A, TIM1_CH3`                        |
|`PA11`|J1/J6  |`USB_DM`   |                                                       |
|`PA12`|J1/J6  |`USB_DP`   |                                                       |
|`PA13`|J3     |`SWDIO`    |`SAI1_SD_B`                                            |
|`PA14`|J3     |`SWCLK`    |`SAI1_FS_B`                                            |
|`PA15`|J7     |GPIO       |`USART2_RX, TIM2_CH1, TIM2_ETR`                        |
|`PB0` |NA     |*Row0*     |                                                       |
|`PB1` |NA     |*Row1*     |                                                       |
|`PB2` |NA     |*Row2*     |                                                       |
|`PB3` |J3     |TRACESWO   |                                                       |
|`PB4` |NA     |*Row3*     |                                                       |
|`PB5` |NA     |*Row4*     |                                                       |
|`PB6` |J2     |`I2C1_SCL` |`USART1_TX`                                            |
|`PB7` |J2     |`I2C1_SDA` |`USART1_RX`                                            |
|`PB8` |J7     |GPIO       |`CAN1_RX, TIM16_CH1`                                   |
|`PB9` |J7     |GPIO       |`CAN1_TX, IR_OUT`                                      |
|`PB10`|J5     |GPIO       |`I2C2_SCL, USART3_TX, LPUART1_RX, TIM2_CH3, TSC_SYNC`  |
|`PB11`|J5     |GPIO       |`I2C2_SDA, USART3_RX, LPUART1_TX, TIM2_CH4`            |
|`PB12`|J5     |`SPI2_NSS` |`SAI1_FS_A, USART3_CK, LPUART1_RTS_DE, TSC_G1_IO1`     |
|`PB13`|J5     |`SPI2_SCK` |`SAI1_SCK_A, USART3_CTS, LPUART1_CTS, I2C2_SCL`        |
|      |       |           |`TIM1_CH1N, TSC_G1_IO2`                                |
|`PB14`|J5     |`SPI2_MISO`|`SAI1_MCLK_A, USART3_RTS_DE, I2C2_SDA, TIM15_CH1`      |
|      |       |           |`TSC_G1_IO3`                                           |
|`PB15`|J5     |`SPI2_MOSI`|`SAI1_SD_A, TIM15_CH2, TSC_G1_IO4`                     |
|`PC13`|NA     |*Led0*     |                                                       |
|`PC14`|NA     |*Led1*     |                                                       |
|`PC15`|NA     |*Led2*     |                                                       |
|`PH0` |NA     |`OSC_IN`   |                                                       |
|`PH1` |J7     |GPIO       |                                                       |
|`PH3` |NA     |*Boot0*    |                                                       |

## Used pins per ports

### J1 - USB-C connector

|Pin   |Name   |Gpio    |
|:-----|:------|:-------|
|A1    |Vbus   |        |
|A4    |GND    |        |
|A5    |CC1    |        |
|B5    |CC2    |        |
|A6    |D+     |PA12    |
|B6    |D+     |        |
|A7    |D-     |PA11    |
|B7    |D-     |        |
|A8    |SBU1   |        |
|B8    |SBU2   |        |

### J2 - Board link connector

|Pin   |Name   |Gpio    |
|:-----|:------|:-------|
|1     |Vbus   |        |
|2     |SDA    |PB7     |
|3     |SCL    |PB6     |
|4     |NC.    |        |
|5     |GND    |        |
|6     |Shield |        |

J2 limited to board-to-board communication only. It use I2C mode by default
but can be configured to use UART instead. This require a cross-cable instead
of a straight one.

### J3 - JTAG connector

|Pin   |Name    |Gpio    |
|:-----|:-------|:-------|
|1     |+3.3V   |        |
|2     |SWDIO   |PA13    |
|3     |GND     |        |
|4     |SWCLK   |PA14    |
|5     |GND     |        |
|6     |TRACESWO|PB3     |
|7     |NC.     |        |
|8     |NC.     |        |
|9     |GND     |        |
|10    |nRST    |        |

### J4 - UART connector

|Pin   |Name    |Gpio    |
|:-----|:-------|:-------|
|1     |+5V     |        |
|2     |RX      |PA10    |
|3     |TX      |PA9     |
|4     |GND     |        |

J4 is serial programming by default but can be changed to I2C bus if J2 is
changed to UART mode.

### J5 - RGB/Oled connector [NP]

|Pin   |Name    |Gpio    |
|:-----|:-------|:-------|
|1     |+3.3V   |        |
|2     |+5V     |        |
|3     |SCK     |PB13    |
|4     |NSS     |PB12    |
|5     |MISO    |PB14    |
|6     |GPIO    |PA8     |
|7     |MOSI    |PB15    |
|8     |OLEDSCL |PB10    |
|9     |OLEDSDA |PB11    |
|10    |GND     |        |

This the the main extension connector. It have `SPI2`, `I2C2`, `USART3`, 
`LPUART1` and many other alternate functions (`SWPMI1_IO`, `TSC_G1_IOx`,
`SAI`).

### J6 - USB Hub connector [NP]

|Pin   |Name   |Gpio    |
|:-----|:------|:-------|
|1     |Vbus   |        |
|2     |Vbus   |        |
|3     |GND    |        |
|4     |D-     |PA11    |
|5     |D+     |PA12    |
|6     |GND    |        |

### J7 - Extension connector [NP]

|Pin   |Name    |Gpio    |
|:-----|:-------|:-------|
|1     |+5V     |        |
|2     |+3.3V   |        |
|3     |GPIO    |PA7     |
|4     |GPIO    |PA15    |
|5     |GPIO    |PH1     |
|6     |GPIO    |PB8     |
|7     |GND     |        |
|8     |GPIO    |PB9     |
